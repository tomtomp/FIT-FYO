\documentclass[10pt,xcolor=pdflatex]{beamer}
\usepackage{newcent}
\usepackage[utf8]{inputenc}
\usepackage[czech]{babel}
\usepackage{hyperref}
\usepackage{fancyvrb}
\usepackage[backend=biber]{biblatex}
\bibliography{doc.bib}

% Removes icon in bibliography
\setbeamertemplate{bibliography item}{}
\DeclareFieldFormat{url}{\href{#1}{\underline{URL}}}

\usetheme{FIT}

\newcommand{\todo}[1]{\textcolor{red}{\textbf{[[#1]]}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title[Aberace čoček]{Aberace čoček}

\author[]{Tomáš Polášek}

\institute[]{Vysoké Učení Technické, Fakulta Informačních Technologií\\
Bo\v{z}et\v{e}chova 1/2. 612 66 Brno - Kr\'alovo Pole\\
xpolas34@stud.fit.vutbr.cz}

%\date{January 1, 2016}
\date{\today}
%\date{} % bez data

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\boldmath

\frame[plain]{\titlepage}

\begin{frame}\frametitle{Paprsková optika}
    \begin{block}{Prostředí}
    	\textbf{Izotropní} a \textbf{Homogenní} \\
    	Index lomu $n = \frac{c}{v}$
    \end{block}
    \begin{block}{Zákony}
    	Odraz $\theta_{i} = \theta_{r}$ \\
    	Lom $n_{i}\ sin\ \theta_{i} = n_{r}\ sin\ \theta_{r}$ \\
        Reverzibilita paprsků
    \end{block}
    \begin{alertblock}{Optika třetího řádu}
    	Bez paraxiální aproximace $sin\ \varphi \neq \varphi$
    \end{alertblock}
\end{frame}

\begin{frame}\frametitle{Aberace čoček}
	Nepokryté \emph{Gausovskou optikou} \\
    Dvě kategorie: \\
    \begin{itemize}
    	\item \emph{Monochromatické}
        \item \emph{Chromatické}
    \end{itemize}
    Aberace prvního a třetího (\emph{Seidelovy}) řádu
\end{frame}

\begin{frame}\frametitle{Zobrazení čočkou}
    \emph{Paraxiální prostor} \\
    \emph{Bodový obraz} $\rightarrow$ deformovaný \emph{Airyho disk} \\
    
    \vfill
    \begin{columns}
    	\column{0.7\textwidth}
    	\centering
    	\includegraphics[width=\textwidth]{img/Paraxial}
    	\column{0.3\textwidth}
    	\centering
    	\includegraphics[width=\textwidth]{img/AiryDisc}
    \end{columns}
\end{frame}

\begin{frame}\frametitle{Rozostření a sklon}
	\emph{Monochromatická} aberace \emph{prvního řádu} \\
    Rozmazání obrazu \\
    Náprava $\rightarrow$ seřízení optického systému \\
    \vfill
    \begin{columns}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics[width=\textwidth]{img/Tilt}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics[width=\textwidth]{img/Defocus}
    \end{columns}
\end{frame}

\begin{frame}\frametitle{Sférická aberace}
	\emph{Monochromatická} aberace \emph{třetího řádu} \\
    Body jsou zobrazeny do podoby Airyho disku \\
    Náprava $\rightarrow$ \emph{kruh nejmenšího zmatku}, asférická čočka\\
    \vfill
    \begin{columns}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics[width=\textwidth]{img/Spherical}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics<2->[width=\textwidth]{img/Costar}
    \end{columns}
\end{frame}

\begin{frame}\frametitle{Komatická aberace}
    Zvětšení \emph{závislé na vzdálenosti} objektu od optické osy \\
    Náprava $\rightarrow$ konvexně-planární čočka, pozice clony \\
    Kruhovité rozmazání od, nebo k optické ose \\
    \vfill
    \begin{columns}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics[width=\textwidth]{img/Coma}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics<2->[width=\textwidth]{img/ComaExample}
    \end{columns}
\end{frame}

\begin{frame}\frametitle{Astigmatismus}
	Nerovnoměrný dopad kužele paprsků \\
    \emph{Horizontální} a \emph{vertikální} ohnisko \\
    Tangenciální (od středu) a sagitální (kolem středu) rozmazání\\
    Náprava $\rightarrow$ přiblížení objektu k optické ose, bod nejmenšího rozmazání\\
    \vfill
    \begin{columns}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics[width=\textwidth]{img/Astigmatism}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics<2->[width=\textwidth]{img/AstigmatismExample}
    \end{columns}
\end{frame}

\begin{frame}\frametitle{Zakřivení ohniska a zkreslení}
	Zakřivení ohniskové roviny mimo \emph{paraxiální oblast} \\
    \emph{Pozitivní} a \emph{negativní}, navzájem se ruší \\
    Zvětšení není konstantní s vzdáleností od optické osy \\
    Přidání zrcadlové kopie systému za clonu \\
    \vfill
    \begin{columns}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics[width=\textwidth]{img/FieldCurvature}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics[width=\textwidth]{img/Distortion}
    \end{columns}
\end{frame}

\begin{frame}\frametitle{Chromatická aberace}
	Projevuje se pouze pro \emph{polychromatické světlo} \\
	\emph{Posun ohnisek} barevných složek \\
    Index lomu klesá s vlnovou délkou \\
    Náprava $\rightarrow$ \emph{achromatický dublet} umožňuje překryv dvou složek\\
    \vfill
    \begin{columns}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics[width=\textwidth]{img/Chromatic}
    	\column{0.5\textwidth}
    	\centering
    	\includegraphics<2->[width=\textwidth]{img/ChromaticExample}
    \end{columns}
\end{frame}

\begin{frame}\frametitle{Srovnání aberací}
    \includegraphics[width=\textwidth]{img/Comparison}
\end{frame}

\bluepage{Vizualizace}

\begin{frame}\frametitle{Zdroje}
	\nocite{OpticsHecht}
    \nocite{RayAberrations}
	\nocite{Examples}
	\nocite{OpticalAberrations}
	\printbibliography
\end{frame}

\end{document}
