/**
 * @file Types.h
 * @author Tomas Polasek
 * @brief Common types and includes.
 */

#ifndef TYPES_H
#define TYPES_H

// Standard C++ library:
#include <exception>
#include <functional>

// Qt includes:
#include <QtDebug>

// Globally available repository of OpenGL functions.
#include "OpenGL.h"

// Configuration and default values.
#include "Config.h"

#endif // TYPES_H
