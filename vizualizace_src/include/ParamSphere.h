/**
 * @file ParamSphere.h
 * @author Tomas Polasek
 * @brief Parametrized sphere model.
 */

#ifndef PARAMSPHERE_H
#define PARAMSPHERE_H

#include "Types.h"
#include "Renderable.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Renderable parametrized sphere.
    class ParamSphere : public Renderable
    {
    public:
        /// Default number of rings per arc.
        static constexpr std::size_t DEFAULT_NUM_RINGS{12u};
        /// Default number of sectors per ring.
        static constexpr std::size_t DEFAULT_NUM_SECTORS{24u};

        /**
         * Construct the sphere without the required buffers.
         * @param focus1 Position of the first focus.
         * @param radius1 Radius of the first arc.
         * @param focus2 Positoin of the second focus.
         * @param radius2 Radius of the second arc.
         * @param thickness Thicknes between arcs.
         * @param rings Number of rings per arc.
         * @param sectors number of sectors per one ring.
         */
        ParamSphere(float focus1, float radius1, float focus2, float radius2, float thickness,
                    std::size_t rings = DEFAULT_NUM_RINGS, std::size_t sectors = DEFAULT_NUM_SECTORS);
        virtual ~ParamSphere();

        /**
         * Set generation parameters.
         * @param focus1 Position of the first focus.
         * @param radius1 Radius of the first arc.
         * @param focus2 Positoin of the second focus.
         * @param radius2 Radius of the second arc.
         * @param thickness Thicknes between arcs.
         * @param rings Number of rings per arc.
         * @param sectors number of sectors per one ring.
         * @warning Does NOT generate any OpenGL buffers, destroy->create is needed.
         */
        void setParameters(float focus1, float radius1, float focus2, float radius2, float thickness,
                           std::size_t rings = DEFAULT_NUM_RINGS, std::size_t sectors = DEFAULT_NUM_SECTORS);

        /**
         * Create all necessary buffers and fill them.
         * The target context has to be already current.
         */
        virtual void create() override;

        /**
         * Render this object in current context.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void render() override;

        /**
         * Destroy OpenGL objects, they can be
         * re-created with create function.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void destroy() override;
    private:
        /// Helper structure used for access to vertex components.
        struct alignas(GLfloat) VertexView
        {
            /// Position: x, y, z
            GLfloat pos[3];
            /// Normal: nx, ny, nz
            GLfloat normal[3];
            /// UV: u, v
            GLfloat uv[2];
        }; // struct VertexView

        /**
         * Components per one vertex:
         *   Position: 3 floats
         *   Normal:   3 floats
         *   UV coords:2 floats
         */
        static constexpr std::size_t COMP_PER_VERTEX{3 + 3 + 2};
        static_assert(sizeof(VertexView) == COMP_PER_VERTEX * sizeof(GLfloat), "Should be equal");
        /// Number of indices per one primitive. 6 for a quad made from 2 triangles.
        static constexpr std::size_t INDICES_PER_PRIM{6};
        /// How many rings should there be for the width ring.
        static constexpr std::size_t WIDTH_BAND_RINGS{2};

        /// Generate the parametrized sphere using set parameters.
        void generateSphere();

        /// The vertex array ID.
        GLuint mVa;
        /// The vertex buffer ID.
        GLuint mVb;
        /// The index buffer ID.
        GLuint mEb;

        /// Buffer containing the vertex data.
        std::vector<GLfloat> mVertexData;
        /// Buffer containing the indices used for indexing vertex data.
        std::vector<GLuint> mIndexData;

        /// Position of the first focus.
        float mFocus1;
        /// Radius of the first arc.
        float mRad1;
        /// Position of the second focus.
        float mFocus2;
        /// Radius of the second arc.
        float mRad2;
        /// Thickness between arcs.
        float mThickness;
        /// Number of rings per arc.
        std::size_t mRings;
        /// Number of sectors per one ring.
        std::size_t mSectors;
    protected:
    }; // class Cube
} // namespace Util

#endif // PARAMSPHERE_H
