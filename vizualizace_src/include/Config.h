/**
 * @file Config.h
 * @author Tomas Polasek
 * @brief Configuration and default values.
 */

#ifndef CONFIG_H
#define CONFIG_H

namespace Config
{
    // Image tab:
    static constexpr float IMAGE_SIZE{1.0f};
    static constexpr float IMAGE_ROT_X{0.0f};
    static constexpr float IMAGE_ROT_Y{0.0f};
    static constexpr float IMAGE_ROT_Z{0.0f};
    static constexpr float IMAGE_POS_X{0.0f};
    static constexpr float IMAGE_POS_Y{0.0f};
    static constexpr float IMAGE_POS_Z{0.0f};

    // Camera tab:
    static constexpr float CAMERA_POS_X{0.0f};
    static constexpr float CAMERA_POS_Y{0.0f};
    static constexpr float CAMERA_POS_Z{-4.0f};
    static constexpr bool CAMERA_PERSPECTIVE{true};
    static constexpr float CAMERA_FOV{60.0f};
    static constexpr float CAMERA_ORHTO_DIV{200.0f};
    static constexpr float CAMERA_NEAR{0.1f};
    static constexpr float CAMERA_FAR{10.0f};

    // Lense tab:
    static constexpr float LENSE_SIZE{0.3f};
    static constexpr float LENSE_ROT_X{0.0f};
    static constexpr float LENSE_ROT_Y{0.0f};
    static constexpr float LENSE_ROT_Z{0.0f};
    static constexpr float LENSE_POS_X{0.0f};
    static constexpr float LENSE_POS_Y{0.0f};
    static constexpr float LENSE_POS_Z{-2.0f};
    static constexpr float LENSE_REFR_INDEX{1.5};
    static constexpr bool LENSE_CHROMATIC_ABERRATION{false};
    static constexpr float LENSE_ABBE_NUMBER{40};

    static constexpr bool LENSE_CUBE_ENABLED{true};
    static constexpr float LENSE_SPHERE_FOC1{1.0f};
    static constexpr float LENSE_SPHERE_RAD1{1.0f};
    static constexpr float LENSE_SPHERE_FOC2{1.0f};
    static constexpr float LENSE_SPHERE_RAD2{1.0f};
    static constexpr float LENSE_SPHERE_THICK{0.0f};
    static constexpr std::size_t LENSE_SPHERE_RINGS{24};
    static constexpr std::size_t LENSE_SPHERE_SECTORS{48};

    // Environment tab:
    static constexpr float ENV_REFR_INDEX{1.0};
} // namespace Config

#endif // CONFIG_H
