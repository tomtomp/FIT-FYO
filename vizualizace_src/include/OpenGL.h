/**
 * @file OpenGL.h
 * @author Tomas Polasek
 * @brief Globally accesible object containing OpenGL functions.
 */

#ifndef OPENGL_H
#define OPENGL_H

#include <QOpenGLFunctions_4_5_Core>

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Globally accesible OpenGL function repository.
    extern QOpenGLFunctions_4_5_Core gl;
} // namespace Util

#endif // OPENGL_H
