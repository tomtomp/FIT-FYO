/**
 * @file ModelMatrix.h
 * @author Tomas Polasek
 * @brief Scene camera class.
 */

#ifndef MODELMATRIX_H
#define MODELMATRIX_H

#include "Types.h"

#include <QVector>
#include <QMatrix4x4>
#include <QQuaternion>

/// Namespace containing utility functions and classes.
namespace Util
{
    class ModelMatrix
    {
    public:
        /// Initialize default values.
        ModelMatrix() = default;
        ~ModelMatrix() = default;

        /**
         * Set position of the model.
         * @param pos New position.
         */
        void setPosition(const QVector3D &pos)
        { mPosition = pos; }

        /**
         * Set position of the model.
         * @param x Position on the X-axis.
         * @param y Position on the Y-axis.
         * @param z Position on the Z-axis.
         */
        void setPosition(float x, float y, float z)
        { mPosition = {x, y, z}; }

        /**
         * Set rotation of the model using quaternion.
         * @param rot New rotation.
         */
        void setRotation(const QQuaternion &rot)
        { mRotation = rot; }

        /**
         * Set scale of the model.
         * @param scale New scale.
         */
        void setScale(const QVector3D &scale)
        { mScale = scale; }

        /**
         * Set scale of the model.
         * @param sx Scale on the X-axis.
         * @param sy Scale on the Y-axis.
         * @param sz Scale on the Z-axis.
         */
        void setScale(float sx, float sy, float sz)
        { mScale = {sx, sy, sz}; }

        /**
         * Set same values for scale on each of the axes.
         * @param scale New scale for all axes.
         */
        void setScale(float scale)
        { setScale(scale, scale, scale); }

        /**
         * Manually recalculate the model matrix.
         */
        void recalculate();

        /**
         * Get the model matrix.
         * @return Returns reference to the model matrix.
         * @warning Does NOT automatically recalculate model matrix!
         */
        const QMatrix4x4 &model() const
        { return mModel; }

        /// Get position.
        QVector3D position() const
        { return mPosition; }

        /// Get Euler rotation angles.
        QVector3D eulerRotation() const
        { return mRotation.toEulerAngles(); }

        /// Get scale.
        QVector3D scale() const
        { return mScale; }
    private:
        /// Position of the model.
        QVector3D mPosition;
        /// Rotation of the model
        QQuaternion mRotation;
        /// Scale of the model.
        QVector3D mScale;
        /// Model matrix itself, calculated from position, rotation and scale.
        QMatrix4x4 mModel;
    protected:
    }; // class ModelMatrix
} // namespace Util

#endif // MODELMATRIX_H
