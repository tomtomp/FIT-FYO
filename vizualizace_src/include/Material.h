/**
 * @file Material.h
 * @author Tomas Polasek
 * @brief Class used for description of lense materials.
 */

#ifndef MATERIAL_H
#define MATERIAL_H

#include "Types.h"

#include <QVector3D>

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Description of lense material
    class Material
    {
    public:
        Material() = default;
        virtual ~Material() = default;

        void setBaseRefractiveIndex(float newIndex);
        void setAbbeNumber(float newAbbe);
        void setChromaticAberration(bool enabled);

        float baseRefractiveIndex() const
        { return mBaseRefractiveIndex; }
        bool chromaticAberration() const
        { return mChromaticAberration; }

        /**
         * Calculate aberrated refreactive indices
         * using Abbe number specifier before.
         * @return Returns vector containing refractive indices
         *   in order RED, GREEN, BLUE.
         * @warning Contains valid values only if chromatic
         *   aberration is set to enabled.
         */
        QVector3D aberratedIndices() const
        { return mAberratedIndices; }
    private:
        /// Wavelength of the n_d refractive index in the Abbe formula
        static constexpr float WAVELENGTH_ND{589.0f};
        /// Wavelength of the n_f refractive index in the Abbe formula
        static constexpr float WAVELENGTH_NF{486.0f};
        /// Wavelength of the n_c refractive index in the Abbe formula
        static constexpr float WAVELENGTH_NC{656.0f};
        /// How much of the change should be attributed to the blue color.
        static constexpr float BLUE_CHANGE{12.0f};
        /// How much of the change should be attributed to the red color.
        static constexpr float RED_CHANGE{4.0f};

        /// Recalculate the aberrated refractive indices.
        void recalculateAberration();

        /// Base refractive index used for calculations.
        float mBaseRefractiveIndex{1.0f};
        /// Abbe number for this material, used when chromatic aberration is enabled.
        float mAbbeNumber{1.0f};
        /// Specifies, whether the chromatic aberration is enabled.
        bool mChromaticAberration{false};
        /// Pre-calculated aberrated indices ordered RED, GREEN, BLUE.
        QVector3D mAberratedIndices;
    protected:
    }; // class Material
} // namespace Util

#endif // MATERIAL_H
