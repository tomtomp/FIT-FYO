/**
 * @file ImageSrc.h
 * @author Tomas Polasek
 * @brief Source image being displayed in the OpenGL.
 */

#ifndef IMAGESRC_H
#define IMAGESRC_H

#include "Types.h"
#include "ModelMatrix.h"
#include "Plane.h"
#include "SceneObject.h"

#include <QImage>
#include <QOpenGLTexture>
#include <QOpenGLShaderProgram>
#include <QOpenGLPixelTransferOptions>
#include <QRgb>

#include <QImageWriter>

/// Namespace containing utility functions and classes.
namespace Util
{
    /**
     * Represents the canvas being displayed in the scene.
     * Combines QImage and QOpenGLTexture used for preparing
     * source images.
     */
    class ImageSrc : public Util::SceneObject
    {
    public:
        /**
         * Initialize empty or default texture.
         * @param loadDefault Should the default image be loaded?
         */
        ImageSrc(bool loadDefault = true);
        virtual ~ImageSrc();

        /**
         * Set new source image.
         * @param img New source image.
         * @param destroy If the format changed, this flag should be set to true.
         * @warning Does NOT upload the image to GPU, only sets the flag!
         * @warning Moves the provided image, destroying it!
         */
        void setImage(QImage &&img, bool destroy);

        /// Set the image to default testing texture.
        void setDefaultImage();

        /**
         * Create the OpenGL resources used in rendering of this image.
         */
        virtual void create() override;

        /**
         * Render the source image.
         * @warning The shader program should already be bound, when called!
         */
        virtual void render() override;

        /**
         * Destroy OpenGL objects, they can be
         * re-created with create function.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void destroy() override;

        /**
         * Set texture unit used by this object.
         * @param textureUnit ID of the texture unit.
         **/
        void setTextureUnit(GLint textureUnit)
        { mTextureUnit = textureUnit; }

        /// Get texture id of the image texture.
        GLuint textureId() const
        { return mTexture.textureId(); }

        /// Get width of the texture.
        int width() const
        { return mTexture.width(); }

        /// Get height of the texture.
        int height() const
        { return mTexture.height(); }

        /**
         * Bind this texture to given texture unit.
         */
        void bind();

        /// Unbind the texture.
        void release();

        /**
         * Set scale of the object in scene world.
         * @param scale Scale set for all axes.
         * @warning Automatically recalculates the model matrix!
         */
        virtual void setScale(float scale) override;
    private:
        /// Format of the default image.
        static constexpr QImage::Format DEFAULT_IMAGE_FORMAT{QImage::Format_RGBA8888};
        /// Width of the default image.
        static constexpr int DEFAULT_IMAGE_WIDTH{1024u};
        /// Height of the default image.
        static constexpr int DEFAULT_IMAGE_HEIGHT{1024u};
        /// Checker size will be 2^CHECKER_MULTIPLE pixels.
        static constexpr int DEFAULT_IMAGE_CHECKER_MULTIPLE{6u};

        /// Load the default testing pattern.
        void loadDefaultImage();

        /// Set default position, scale and rotation of the iamge.
        void setDefaultPosition();

        /**
         * Verify that a texture is uploaded otherwise upload it.
         */
        void checkUpload();

        /// Contains currentl image.
        QImage mImage;
        /// Flag specifying whether the current image should be uploaded.
        bool mUpload;
        /// Flag specifying whether the format of data has changed and storage should be re-created.
        bool mFormatChanged;
        /// Texture unit used by this object.
        GLuint mTextureUnit;
        /// Texture container.
        QOpenGLTexture mTexture;
        /// Plane used for drawing the image.
        Util::Plane mPlane;
    protected:
    }; // class ImageSrc
} // namespace Util

#endif // IMAGESRC_H
