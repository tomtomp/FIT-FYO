/**
 * @file RenderArea.h
 * @author Tomas Polasek
 * @brief Class representing the OpenGL draw area.
 */

#ifndef RENDERAREA_H
#define RENDERAREA_H

#include "Types.h"
#include "Util.h"
#include "Timer.h"
#include "LenseEngine.h"

#include <QOpenGLWidget>
#include <QOpenGLContext>
#include <QPainter>

#include <QTimer>

#include <QKeyEvent>
#include <QMouseEvent>
#include <QWheelEvent>

class RenderArea : public QOpenGLWidget
{
    Q_OBJECT

public:
    explicit RenderArea(QWidget *parent = 0);
    ~RenderArea();

    /// Start periodically redrawing this area.
    void startRenderer();

    /// Stop periodically redrawing this area.
    void stopRenderer();

    /// Set source image to the default testing pattern.
    void setDefaultImage();

    /**
     * Set new source image.
     * @param image Image to use.
     * @param destroy Should the texture be destroyed and re-created?
     */
    void setImage(QImage &&image);

    /**
     * Set source image to given video frame.
     * @param frame Frame to use.
     * @param formatChanged Did the format of video change, since the last time?
     */
    void setVideoFrame(QImage &&frame, bool formatChanged);

    void setImageSize(float size)
    { mEngine->setImageSize(size); }
    void setImageRotation(float rotX, float rotY, float rotZ)
    { mEngine->setImageRotation(rotX, rotY, rotZ); }
    void setImagePosition(float posX, float posY, float posZ)
    { mEngine->setImagePosition(posX, posY, posZ); }

    void setCameraPosition(float posX, float posY, float posZ)
    { mEngine->setCameraPosition(posX, posY, posZ); }
    void setCameraProjection(float fov)
    { mEngine->setCameraProjection(fov); }
    void setCameraOrthographic()
    { mEngine->setCameraOrthographic(); }

    void setLenseSize(float size)
    { mEngine->setLenseSize(size); }
    void setLenseRotation(float rotX, float rotY, float rotZ)
    { mEngine->setLenseRotation(rotX, rotY, rotZ); }
    void setLensePosition(float posX, float posY, float posZ)
    { mEngine->setLensePosition(posX, posY, posZ); }
    void setLenseRefrIndex(float refrIndex)
    { mEngine->setLenseRefrIndex(refrIndex); }
    void setLenseCA(bool enabled, float abbe)
    { mEngine->setLenseChromaticAberration(enabled, abbe); }
    void setLenseTypeCube()
    { mEngine->setLenseTypeCube(); }
    void setLenseTypeSphere(float foc1, float radius1, float foc2, float radius2, float thickness, std::size_t rings, std::size_t sectors)
    { mEngine->setLenseTypeSphere(foc1, radius1, foc2, radius2, thickness, rings, sectors); }
    void setLenseModel(QFile &input)
    { mEngine->setLenseModel(input); }

    void setEnvRefrIndex(float refrIndex)
    { mEngine->setEnvRefrIndex(refrIndex); }

    /// Used for connecting signals from engine to the UI.
    const Engine::LenseEngine *engine() const
    { return mEngine; }

signals:
    void engineInitialized();

private:
    /// Major version of OpenGL, using OpenGL 4.5.
    static constexpr int DEFAULT_GL_VERSION_MAJOR{4};
    /// Minor version of OpenGL, using OpenGL 4.5.
    static constexpr int DEFAULT_GL_VERSION_MINOR{5};
    /// OpenGL profile, we will use the Core profile.
    static constexpr QSurfaceFormat::OpenGLContextProfile DEFAULT_GL_PROFILE{QSurfaceFormat::CoreProfile};
    /// Frame swapping behavior, using double buffer.
    static constexpr QSurfaceFormat::SwapBehavior DEFAULT_GL_SWAP_BEHAVIOR{QSurfaceFormat::DoubleBuffer};
    /// Swap interval, enable V-Sync.
    static constexpr int DEFAULT_GL_SWAP_INTERVAL{1};
    /// Number of samples for multisampling.
    static constexpr int DEFAULT_GL_SAMPLES{4};
    /// How long between timer triggered updates.
    static constexpr int DEFAULT_TIMER_MSEC{1};
    /// How many times per second should the scene be updated.
    static constexpr uint16_t DEFAULT_UPS{30u};
    /// How many frames per second should be the target.
    static constexpr uint16_t DEFAULT_FPS{60u};
    /// Number of milliseconds per one second
    static constexpr float MS_PER_S{1000.0f};
    /// Number of microseconds per one millisecond
    static constexpr float US_PER_MS{1000.0f};
    /// How many milliseconds between FPS and UPS calculations.
    static constexpr float TIME_PER_RECALC{MS_PER_S};

    /// Initialization of OpenGL objects.
    virtual void initializeGL() override;
    /// Triggered when viewport changes size.
    virtual void resizeGL(int width, int height) override;
    /// Triggered when painting is requested.
    virtual void paintGL() override;

    /// Set all required context settings.
    void setupContext();

    /// Setup re-drawing timer.
    void setupTimer();

    /**
     * Called when update event from renderTimer is triggered.
     * Updates state of the scene and attempts to keep updates
     * per second and frames per second to requested value.
     */
    void updateState();

    /// Render overlay with debug information.
    void renderOverlay();

#ifdef QT_DEBUG
    /// OpenGL debug callback, triggered when message is sent.
    static void glDebugCallbackFun(GLenum source,
                                   GLenum type,
                                   GLuint id,
                                   GLenum severity,
                                   GLsizei length,
                                   const GLchar* message,
                                   const void* userParam);
#endif

    /**
     * Event triggered when a key is pressed.
     * @param event Event information.
     */
    virtual void keyPressEvent(QKeyEvent *event) override;

    /**
     * Event triggered when a key is released.
     * @param event Event information.
     */
    virtual void keyReleaseEvent(QKeyEvent *event) override;

    /**
     * Event triggered when mouse is moved.
     * @param event Event information.
     */
    virtual void mouseMoveEvent(QMouseEvent *event) override;

    /**
     * Event triggered when mouse key is pressed.
     * @param event Event information.
     */
    virtual void mousePressEvent(QMouseEvent *event) override;

    /**
     * Event triggered when mouse key is released.
     * @param event Event information.
     */
    virtual void mouseReleaseEvent(QMouseEvent *event) override;

    /**
     * Event triggered when wheel is turned.
     * @param event Event information.
     */
    virtual void wheelEvent(QWheelEvent *event) override;

    /// Timer used for triggering re-draws.
    QTimer *mRenderTimer;
    /// Target time between updates in milliseconds.
    float mTargetUT{0.0f};
    /// Target time between frames in milliseconds.
    float mTargetFT{0.0f};
    /// Time since the last update in milliseconds.
    float mUpdateLagMs{0.0f};
    /// Time since the last frame in milliseconds.
    float mFrameLagMs{0.0f};
    /// Number of frames since FPS calculation.
    uint64_t mFrameCounter{0u};
    /// Number of updates since UPS calculation.
    uint64_t mUpdateCounter{0u};
    /// Approximate time per one frame.
    float mFrameTime{0.0f};
    /// Actually rendered frames per second.
    float mFps{0.0f};
    /// Actually performed updates per second.
    float mUps{0.0f};
    /// Timer used for measuring elapsed time since last iteration.
    Util::Timer mElapsedTimer;
    /// Timer used for measuring time between FPS and UPS calculations.
    Util::Timer mCounterTimer;
    /// Flag used for storing information if we are presenting video.
    bool mPresentingVideo;

    /// Engine containing the scene.
    Engine::LenseEngine *mEngine;
protected:
    /**
     * Contains OpenGL commands which render the scene.
     * Called with already active context.
     */
    virtual void render();
}; // class RenderArea

#endif // RENDERAREA_H
