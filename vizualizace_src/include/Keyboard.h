/**
 * @file Keyboard.h
 * @author Tomas Polasek
 * @brief Keyboard handling class.
 */

#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "Types.h"

#include <QEvent>
#include <QKeySequence>

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Keyboard handling class.
    class Keyboard
    {
    public:
        /// Actions possible with keys.
        enum Action
        {
            ACTION_PRESS = 0,
            ACTION_RELEASE,
            ACTION_REPEAT,
        };

        /// Type of the action function, called when corresponding key is pressed.
        using ActionFun = std::function<void()>;
        /// Type of the default action, arguments - key, mods, action.
        using DefaultActionFun = std::function<void(Qt::Key, Qt::KeyboardModifiers, Action)>;

        Keyboard() = default;
        ~Keyboard() = default;

        /**
         * Set action for given key combination.
         * @param key Key - e.g. Qt::Key_Up.
         * @param mods Modifiers - e.g. Qt::ShiftModifier.
         * @param action Action taken with the key - e.g. Keyboard::ACTION_PRESS.
         * @param fun Function to call.
         */
        void setAction(Qt::Key key, Qt::KeyboardModifiers mods, Action action, ActionFun fun)
        { mMapping[{key, mods, action}] = fun; }

        /**
         * Reset action for given key combination.
         * @param key Key - e.g. Qt::Key_Up.
         * @param mods Modifiers - e.g. Qt::ShiftModifier.
         * @param action Action taken with the key - e.g. Keyboard::ACTION_PRESS.
         */
        void setAction(Qt::Key key, Qt::KeyboardModifiers mods, Action action)
        { mMapping.erase({key, mods, action}); }

        /**
         * Set the default action, called when no other mapping is found.
         * Function will be passed following arguments :
         *  int key - Key pressed, e.g. Qt::Key_Up.
         *  int action - Action of the key - e.g. Qt::ShiftModifier.
         *  int mods - Mods - e.g. Keyboard::ACTION_PRESS.
         * @param fun Function to call.
         */
        void setDefaultAction(DefaultActionFun fun)
        { mDefaultAction = fun; }

        /**
         * Reset the default action.
         */
        void resetDefaultAction()
        { mDefaultAction = nullptr; }

        /**
         * Dispatch keyboard event and trigger action function.
         * @param key Keyboard key.
         * @param mods Key modifiers.
         * @param action ACTION_PRESS, ACTION_RELEASE or ACTION_REPEAT.
         */
        void dispatch(Qt::Key key, Qt::KeyboardModifiers mods, Action action);

        /// Print debug information about given keypress.
        static void debugPrintKeyAction(Qt::Key key, Qt::KeyboardModifiers mods, Action action);

        /**
         * Convert action to string representation.
         * @param action Action to convert to string.
         * @return Returns string representation of provided action.
         */
        static const char *actionToStr(Action action);
    private:
        /// Helper structure for searching in map.
        struct KeyCombination
        {
            KeyCombination(Qt::Key keyV, Qt::KeyboardModifiers modsV, Action actionV) :
                key{keyV}, mods{modsV}, action{actionV} { }

            /// Keycode.
            Qt::Key key;
            /// Modifiers.
            Qt::KeyboardModifiers mods;
            /// Action - e.g. ACTION_PRESS.
            Action action;

            /// Comparison operator.
            bool operator<(const KeyCombination &rhs) const
            { return (key < rhs.key) || ((key == rhs.key) && (mods < rhs.mods)) || ((key == rhs.key) && (mods == rhs.mods) && (action < rhs.action)); }
            /// Comparison equal operator.
            bool operator==(const KeyCombination &rhs) const
            { return (key == rhs.key) && (mods == rhs.mods) && (action == rhs.action); }
        }; // struct KeyCombination

        /// Mapping from keys to actions.
        std::map<KeyCombination, ActionFun> mMapping;
        /// Default action, called when no mapping is found.
        DefaultActionFun mDefaultAction;
    protected:
    }; // class Keyboard
} // namespace Util


#endif //KEYBOARD_H
