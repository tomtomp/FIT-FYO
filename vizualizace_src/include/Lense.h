/**
 * @file Lense.h
 * @author Tomas Polasek
 * @brief Base class for lense objects in the scene.
 */

#ifndef LENSE_H
#define LENSE_H

#include "Types.h"
#include "SceneObject.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Base class for all lense objects in the scene.
    class Lense : public SceneObject
    {
    public:
        Lense() = default;
        Lense(const SceneObject &other) :
            SceneObject(other)
        { }
        virtual ~Lense() = default;
    private:
    protected:
    }; // class Lense
} // namespace Util

#endif // LENSE_H
