/**
 * @file Renderable.h
 * @author Tomas Polasek
 * @brief Base class for renderable OpenGL objects.
 */

#ifndef RENDERABLE_H
#define RENDERABLE_H

#include "Types.h"

#include <QOpenGLShaderProgram>

/// Namespace containing utility functions and classes.
namespace Util
{
    /// Base renderable OpenGL object.
    class Renderable
    {
    public:
        /// Attribute location used for position.
        static constexpr GLuint ATTRIBUTE_LOC_POS{0};
        /// Attribute location used for normals.
        static constexpr GLuint ATTRIBUTE_LOC_NORM{1};
        /// Attribute location used for texture coords.
        static constexpr GLuint ATTRIBUTE_LOC_UV{2};

        Renderable()
        { }
        virtual ~Renderable() = default;

        // No copying.
        Renderable(const Renderable &other) = delete;
        Renderable &operator=(const Renderable &rhs) = delete;
        // No moving.
        Renderable(Renderable &&other) = delete;
        Renderable &operator=(Renderable &&rhs) = delete;

        /**
         * Create all necessary buffers and fill them.
         * The target context has to be already current.
         */
        virtual void create() = 0;

        /// Is the renderable initialized?
        bool created()
        { return mCreated; }

        /**
         * Render this object in current context.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void render() = 0;

        /**
         * Destroy OpenGL objects, they can be
         * re-created with create function.
         * @warning The context has to be the same
         * as the one this object was created in!
         */
        virtual void destroy() = 0;
    private:
    protected:
        /// Is this renderable initialized?
        bool mCreated{false};
    }; // class Renderable
} // namespace Util

#endif // RENDERABLE_H
