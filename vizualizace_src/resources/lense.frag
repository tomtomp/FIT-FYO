#version 450 core

#define M_PI 3.141592653589793238462643383
#define M_2PI 2.0 * 3.141592653589793238462643383

/// Depth map of the lense.
uniform sampler2D depthNormalMap;
/// Image behind the lense.
uniform sampler2D background;
/// Mode used for debugging.
uniform int mode;
/// Projection matrix.
uniform mat4x4 p;
/// Environment refraction index.
uniform float envRI;
/// Lense refraction index.
uniform float lRI;
/// Background image inverted model-view matrix.
uniform mat4x4 bgMvInv;

/// Environment to lense refraction index ratio.
float ETL = envRI / lRI;
/// Lense to environment refraction index ratio.
float LTE = lRI / envRI;

/// Fresnel exponent, used in reflection calculation.
const float FRESNEL_POWER = 5.0;
/// Schlick's factor for environment to lense.
float SCHLICK_R0_ETL = ((envRI - lRI) * (envRI - lRI)) / ((envRI + lRI) * (envRI + lRI));
/// Schlick's factor for lense to environment.
float SCHLICK_R0_LTE = ((envRI - lRI) * (envRI - lRI)) / ((envRI + lRI) * (envRI + lRI));
/// Number of search steps taken, when searching for refracted ray length.
const int SEARCH_STEPS = 10;

noperspective in float fragDist;
noperspective in vec3 fragPos;
noperspective in vec3 fragNorm;
noperspective in vec2 fragUv;

out vec4 outColor;

/**
 * Calculate Fresnel reflection factor using Schlick's approximation.
 * @param r0 Specifying ((n1 - n2) / (n1 + n2))^2 .
 * @param power Fresnel power exponent.
 * @param dir Direction of the ray.
 * @param normal Normal of the surface.
 * @return Returns how much light should be reflected <0, 1>.
 */
float fresnel(in const float r0, in const float power, in vec3 dir, in vec3 normal)
{
    /*
     * Formula:
     *
     * R(Theta) = R_0 + (1 - R_0) * (1 - cos(Theta))^FresnelPower
     * R_0 = ((n1 - n2) / (n1 + n2))^2
     * cos(Theta) = dot(-dir, normal);
     */

    return r0 + (1.0f - r0) * (1.0f - dot(-dir, normal));
}

/**
 * Calculate length of ray before it intersects with the back plane.
 * @param min Minimal distance of intersection searched.
 * @param max Maximal distance of intersection searched.
 * @param pos Position where the ray starts at.
 * @param ray Ray being checked.
 * @return Returns length of the ray before it intersects with the back plane.
 */
float findRayLen(in const float min, in const float max, in vec3 pos, in vec3 ray)
{
    const float baseDist = length(pos);
    const float distChange = length(pos + ray) - baseDist;

    // Initial values.
    float beg = min;
    float end = max;
    float mid;

    // Binary search.
    for (int step = 0; step < SEARCH_STEPS; ++step)
    {
        // Middle of the tested interval.
        mid = (end + beg) / 2.0f;
        // New length of the refracted ray.
        float dn = (mid / distChange - baseDist);

        // Calculate the ray end point.
        vec3 rayEnd = pos + dn * ray;
        // Calculate position in the backface depth map.
        vec4 screenPos = p * vec4(rayEnd, 1.0f);
        // TODO - Do we need this line?
        screenPos /= screenPos.w;
        screenPos = (screenPos + 1.0f) / 2.0f;

        // Get depth from the depth map.
        float depthSample = texture2D(depthNormalMap, screenPos.xy).w;

        //if (depthSample < (screenPos.z))
        if (depthSample < length(rayEnd))
        { // The interface is in the top part.
            // beg = beg;
            end = mid;
        }
        else
        { // The interface is in the bottom part.
            beg = mid;
            // end = end;
        }
    }

    // The final result is the middle of the interval.
    return ((end + beg) / 2.0f) - baseDist;
}

vec2 mvPosToMap(in vec3 pos)
{
    vec4 mapPos = p * vec4(pos, 1.0f);
    // TODO - Do we need this line?
    mapPos /= mapPos.w;
    // Move to <-1.0, 1.0> -> <0.0, 1.0>
    return (mapPos.xy + 1.0) / 2.0;
}

float refractInterface(in vec3 inPos, in vec3 inNorm, in vec3 inRay, in float ito, in float schlick, out vec3 outPos, out vec3 outRay)
{
    // Sample of the second interface, along the first ray.
    vec4 depthNormalSample = texture2D(depthNormalMap, mvPosToMap(inPos));
    // Distance of the second interface, along the first ray.
    float otherDist = depthNormalSample.w;

    // Reflectance on the first interface.
    float inReflectance = fresnel(schlick, FRESNEL_POWER, inRay, inNorm);
    // Refracted ray on the first interface.
    vec3 inRefract = normalize(refract(inRay, inNorm, ito));

    // Find ray length, where it intersects the interface.
    float dr = findRayLen(length(inPos), depthNormalSample.w + 0.5f, fragPos, inRefract);
    // Calculate point where the second refraction happens.
    outPos = inPos + dr * inRefract;
    outRay = normalize(inRefract);

    return inReflectance;
}

float refractLast(in vec3 inNorm, in vec3 inRay, in float ito, in float schlick, out vec3 outRay)
{
    // Calculate the refraction on the second point using normal from sample.
    vec3 outRefract = normalize(refract(inRay, inNorm, ito));
    // Reflectance on the second interface.
    float outReflectance = fresnel(schlick, FRESNEL_POWER, inRay, inNorm);

    outRay = outRefract;
    return outReflectance;
}

vec2 rayImageIntersect(in vec3 pos, in vec3 ray)
{
    // Ray in the source image space:
    vec3 srcImagePoint = (bgMvInv * vec4(pos, 1.0f)).xyz;
    vec3 srcImageRay = (bgMvInv * vec4(ray, 0.0f)).xyz;
    // Calculate intersection with the X-Y plane.
    vec3 res = srcImagePoint + srcImageRay * (-srcImagePoint.z / srcImageRay.z);

    // Move from model space to texture space.
    return (res.xy + 1.0f) / 2.0f;
}

void main()
{
    vec3 secPos;
    vec3 secRay;
    float firstReflectance = refractInterface(fragPos, fragNorm, normalize(fragPos), ETL, SCHLICK_R0_ETL, secPos, secRay);

    vec3 outRay;
    vec3 secNorm = -texture2D(depthNormalMap, mvPosToMap(secPos)).xyz;
    float secReflectance = refractLast(secNorm, secRay, LTE, SCHLICK_R0_LTE, outRay);

    vec2 res = rayImageIntersect(secPos, outRay);
    vec4 backgroundSample = texture2D(background, res);

    /*
    // Sample of the second interface, along the first ray.
    vec4 depthNormalSample = texture2D(depthNormalMap, fragUv);
    // Distance of the second interface, along the first ray.
    float otherDist = depthNormalSample.w;

    // Direction of the first ray.
    vec3 firstDir = normalize(fragPos);
    // Reflectance on the first interface.
    float firstReflectance = fresnel(SCHLICK_R0_ETL, FRESNEL_POWER, firstDir, fragNorm);



    // Reflected ray on the first interface.
    vec3 firstReflect = reflect(firstDir, fragNorm);
    // Refracted ray on the first interface.
    vec3 firstRefract = normalize(refract(firstDir, fragNorm, ETL));

    // Find ray length, where it intersects the interface.
    float dr = findRayLen(fragDist, otherDist + 0.5f, fragPos, firstRefract);
    // Calculate point where the second refraction happens.
    vec3 secRefractPoint = fragPos + dr * firstRefract;

    // Calculate position of the point in the backface depth map.
    vec4 secRefractScreenPos = p * vec4(secRefractPoint, 1.0f);
    // TODO - Do we need this line?
    secRefractScreenPos /= secRefractScreenPos.w;
    // Move <-1.0, 1.0> -> <0.0, 1.0>
    secRefractScreenPos = (secRefractScreenPos + 1.0) / 2.0;

    // Get normal and depth information from backface depth map.
    vec4 secRefractSample = texture2D(depthNormalMap, secRefractScreenPos.xy);

    // Calculate the refraction on the second point using normal from sample.
    //vec3 secRefract = refract(normalize(secRefractPoint), -secRefractSample.xyz, LTE);
    vec3 secRefract = refract(normalize(firstRefract), -secRefractSample.xyz, LTE);
    // Reflectance on the second interface.
    float secReflectance = fresnel(SCHLICK_R0_LTE, FRESNEL_POWER, normalize(firstRefract), -secRefractSample.xyz);
    */

    /*
    vec3 outRay;
    vec3 secNorm = -texture2D(depthNormalMap, mvPosToMap(secRefractPoint)).xyz;
    float secReflectance = refractLast(secNorm, normalize(firstRefract), LTE, SCHLICK_R0_LTE, outRay);
    */


    /*
    // Ray in the source image space:
    vec3 srcImagePoint = (bgMvInv * vec4(secRefractPoint, 1.0f)).xyz;
    vec3 srcImageRay = (bgMvInv * vec4(secRefract, 0.0f)).xyz;
    //vec3 srcImageRay = (bgMvInv * vec4(outRay, 0.0f)).xyz;
    // Calculate intersection with the X-Y plane.
    vec3 res = srcImagePoint + srcImageRay * (-srcImagePoint.z / srcImageRay.z);


    // Move from model space to texture space and sample the source image texture.
    vec4 backgroundSample = texture2D(background, (res.xy + 1.0f) / 2.0f);
    */

    /*
    float crit = asin(ETL);
    float thisAng = acos(dot(normalize(-secRefractSample.xyz), normalize(-firstRefract)));

    if (thisAng > crit)
    {
        outColor = vec4(1.0f, 0.0f, 1.0f, 1.0f);
        return;
    }
    */

    /*
    vec3 secondReflect = normalize(reflect(firstRefract, -secRefractSample.xyz));
    float srDr = findRayLen(fragDist, otherDist + 0.5f, secRefractPoint, -secondReflect);
    vec3 thirdRefractPoint = secRefractPoint + srDr * secondReflect;

    vec3 reflOutRay;
    vec3 reflNorm = -texture2D(depthNormalMap, mvPosToMap(thirdRefractPoint)).xyz;
    float thirdReflectance = refractLast(reflNorm, normalize(secondReflect), LTE, SCHLICK_R0_LTE, reflOutRay);

    // Ray in the source image space:
    vec3 srcImagePointR = (bgMvInv * vec4(thirdRefractPoint, 1.0f)).xyz;
    //vec3 srcImageRay = (bgMvInv * vec4(secRefract, 0.0f)).xyz;
    vec3 srcImageRayR = (bgMvInv * vec4(reflOutRay, 0.0f)).xyz;
    // Calculate intersection with the X-Y plane.
    vec3 resR = srcImagePointR + srcImageRayR * (-srcImagePointR.z / srcImageRayR.z);

    vec4 backgroundSampleR = texture2D(background, (resR.xy + 1.0f) / 2.0f);

    //outColor = vec4(reflNorm.xy, -reflNorm.z, 1.0f);
    //return;
    float crit = asin(ETL);
    float thisAng = acos(dot(normalize(reflNorm.xyz), normalize(-secondReflect)));
    if (thisAng > crit)
    {
        outColor = vec4(1.0f, 0.0f, 1.0f, 1.0f);
    }
    if (mode == 0)
    {
        return;
    }
    if (length(secRefract) < 0.01f)
    {
        //outColor = vec4(vec3(srDr), 1.0f);
        outColor = vec4(vec3(1.0f), 1.0f);
    }
    else
    {
        outColor = vec4(vec3(0.0f), 1.0f);
    }
    return;
    */


    if (mode == 0)
    {
        outColor = (1.0f - firstReflectance) * (1.0f - secReflectance) * backgroundSample;
        //outColor = (1.0f - firstReflectance) * (1.0f - secReflectance) * backgroundSample + secReflectance * backgroundSampleR;
    }
    else if (mode == 1)
    {
        outColor = backgroundSample;
        //outColor = vec4(vec3(dr), 1.0f);
        //outColor = vec4(abs(secRefractPoint), 1.0f);
        //outColor = vec4(secRefractSample.xyz, 1.0f);
        //outColor = vec4((secRefractPoint.xy) * 3.0f, 0.0f, 1.0f);
        //outColor = vec4((fragPos.xy) * 3.0f, 0.0f, 1.0f);
        //outColor = vec4(abs(secRefract), 1.0f);
        //outColor = vec4(vec3(length(outRay)), 1.0f);
        //outColor = vec4(vec3(dot(-normalize(firstRefract), secNorm)), 1.0f);
        //outColor = vec4(abs(secRefractPoint.xy) * 3.0f, 0.0f, 1.0f);
        //outColor = vec4(vec3(abs(reflOutRay)), 1.0f);
        //outColor = vec4(vec3(-thirdRefractPoint.z / 3.0f), 1.0f);
        //outColor = vec4(vec3(secondReflect.z), 1.0f);
        //vec2 p = mvPosToMap(thirdRefractPoint);
        //outColor = vec4(vec3(texture2D(depthNormalMap, p).w), 1.0f);
        //outColor = vec4(vec3(srDr), 1.0f);
    }
    else if (mode == 2)
    {
        outColor = vec4(fragDist / 3.0f, fragDist / 3.0f, fragDist / 3.0f, 1.0f);
    }
    else if (mode == 3)
    {
        float otherDist = texture2D(depthNormalMap, fragUv).w;
        outColor = vec4(otherDist / 4.0f, otherDist / 4.0f, otherDist / 4.0f, 1.0f);
    }
    else if (mode == 4)
    {
        float otherDist = texture2D(depthNormalMap, fragUv).w;
        float lengthInside = abs(otherDist - fragDist);
        outColor = vec4(lengthInside / 2.0f, lengthInside / 2.0f, lengthInside / 2.0f, 1.0f);
    }
    else if (mode == 5)
    {
        outColor = vec4(abs(fragNorm), 1.0f);
    }
    else if (mode == 6)
    {
        outColor = vec4(vec3(firstReflectance), 1.0f);
    }
    else
    {
        outColor = vec4(vec3(secReflectance), 1.0f);
    }

    //vec4 backgroundSample = texture2D(background, fragUv);

    /*
    vec3 secDir = normalize(firstRefract);
    float secReflectance = fresnel(SCHLICK_R0_LTE, FRESNEL_POWER, firstDir, fragNorm);
    vec3 secReflect = reflect(firstDir, fragNorm);
    vec3 secRefract = refract(firstDir, fragNorm, LTE);
    */

    /*
    // Start on the position of refraction.
    const float MIN = fragDist;
    // End point is somewhere in the distance...
    //const float MAX = 5.0f;
    const float MAX = otherDist + 0.5f;
    // How many steps should the binary search take.
    const int SEARCH_STEPS = 10;

    // Initial values.
    float beg = MIN;
    float end = MAX;
    float mid;

    // Binary search.
    for (int step = 0; step < SEARCH_STEPS; ++step)
    {
        // Middle of the tested interval.
        mid = (end + beg) / 2.0f;
        // New length of the refracted ray.
        float dn = (mid - fragDist);

        // Calculate position in the backface depth map.
        vec4 screenPos = p * vec4(fragPos + dn * firstRefract, 1.0f);
        // TODO - Do we need this line?
        screenPos /= screenPos.w;
        screenPos = (screenPos + 1.0f) / 2.0f;

        // Get depth from the depth map.
        float depthSample = texture2D(depthNormalMap, screenPos.xy).w;

        //outColor = vec4(abs(vec3(texture2D(depthNormalMap, screenPos.xy).rgb)), 1.0f);
        //outColor = vec4(abs(vec3(texture2D(depthNormalMap, screenPos.xy).a)), 1.0f);
        //outColor = vec4(vec3(depthSample), 1.0f);
        //return;

        // ??
        vec3 t = fragPos + dn * firstRefract;
        //if (depthSample < (screenPos.z))
        if (depthSample < length(t))
        { // The interface is in the top part.
            // beg = beg;
            end = mid;
        }
        else
        { // The interface is in the bottom part.
            beg = mid;
            // end = end;
        }
    }

    // The final result is the middle of the interval.
    float dr = ((end + beg) / 2.0f) - fragDist;
    */

    //vec3 secRefractPoint = fragPos + firstRefract * ((fragPos.z - lengthInside) / firstRefract.z);
    //vec3 secRefract = refract(firstRefract, fragNorm, LTE);

    /*
    // Using transformed axes of the source image:
    vec3 backgroundAxisX = (bgMv * vec4(1.0f, 0.0f, 0.0f, 0.0f)).xyz;
    vec3 backgroundAxisY = (bgMv * vec4(0.0f, 1.0f, 0.0f, 0.0f)).xyz;
    vec2 res = vec2(dot(backgroundAxisX, secRefract), dot(backgroundAxisY, secRefract));
    */

    // TODO - Use image model matrix to get precise point, so it works with rotations and position changes.
    //vec3 res = secRefractPoint + secRefract * ((-secRefractPoint.z - 4.0f) / secRefract.z);

    //vec3 res = fragPos + firstRefract * ((-fragPos.z - 4.0f) / firstRefract.z);
    //res = (res + 1.0f) / 2.0f;
    //backgroundSample = texture2D(background, res.xy);

    //vec4 t = p * vec4(secRefractPos, 1.0f);
    //vec4 t = p * vec4(fragPos, 1.0f);
    //backgroundSample = vec4(abs(texture2D(depthNormalMap, t.xy).rgb), 1.0f);
    //backgroundSample = vec4(t.xy, 0.0f, 1.0f);
    /*
    vec4 t = p * vec4(res, 1.0f);
    t /= t.w;
    t.xyz += 1.0f;
    t.xyz /= 2.0f;
    backgroundSample = vec4(texture2D(background, t.xy).rgb, 1.0f);
    */

    /*
    if (ranOut)
    {
        outColor = vec4(1.0, 0.0, 1.0, 1.0);
    }
    outColor = vec4(vec3(-dr), 1.0);
    if (MAX < MIN)
    {
        outColor = vec4(1.0, 0.0, 0.0, 1.0);
    }
    if (end < beg)
    {
        outColor = vec4(0.0, 1.0, 0.0, 1.0);
    }
    */

    //outColor = vec4(otherDist, otherDist, otherDist, 1.0f);
    //outColor = vec4(1.0f, 0.0f, 1.0f, 1.0f);
    //outColor = vec4(texture2D(depthNormalMap, fragUv).rgb, 1.0f);
    //outColor = vec4(fragDist, otherDist, lengthInside, 1.0f);
}
