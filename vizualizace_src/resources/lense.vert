#version 450 core

/// Model-view matrix.
uniform mat4x4 mv;
/// Model-View-Projection matrix.
uniform mat4x4 mvp;
/// Inverse transpose of model matrix.
uniform mat3x3 invmt;

layout(location = 0) in vec3 vertPos;
layout(location = 1) in vec3 vertNorm;
layout(location = 2) in vec2 vertUv;

out float fragDist;
out vec3 fragPos;
out vec3 fragNorm;
out vec2 fragUv;

void main()
{
    // TODO - Change for orthonormal projection.
    vec4 ray = mv * vec4(vertPos, 1.0);
    // TODO - Do we need this line?
    //ray.xyz /= ray.w;
    //vec3 dir = normalize(ray.xyz / ray.w);

    //fragDist = length(vec3(vec4(mv * vec4(vertPos, 1.0)).xyz));
    fragDist = length(ray.xyz);
    fragPos = ray.xyz;
    fragNorm = normalize(invmt * vertNorm);

    gl_Position = mvp * vec4(vertPos, 1.0);
    fragUv = (gl_Position.xy / gl_Position.w + 1.0f) / 2.0f;
}
