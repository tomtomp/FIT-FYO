#version 450 core

uniform mat4x4 mv;
uniform mat4x4 mvp;
uniform mat3x3 invmt;

layout(location = 0) in vec3 vertPos;
layout(location = 1) in vec3 vertNorm;
layout(location = 2) in vec2 vertUv;

out float fragDist;
out vec3 fragNorm;
out vec2 fragUv;

void main()
{
    // Calculate the distance from camera and normal, which will be written to the framebuffer.
    fragDist = length(vec3(vec4(mv * vec4(vertPos, 1.0)).xyz));
    fragNorm = normalize(invmt * vertNorm);
    fragUv = vertUv;

    gl_Position = mvp * vec4(vertPos, 1.0);
}
