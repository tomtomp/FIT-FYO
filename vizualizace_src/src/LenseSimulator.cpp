/**
 * @file LenseSimulator.cpp
 * @author Tomas Polasek
 * @brief Contains the main function.
 */

#include "LenseSimulatorApp.h"

int main(int argc, char *argv[])
{
    try {
        LenseSimulatorApp app(argc, argv);
        return app.start();
    } catch (const std::runtime_error &err) {
        qCritical() << "Exception escaped the main loop! : " << err.what();
    } catch (...) {
        qCritical() << "Unknown exception escaped the main loop!";
    }

    return EXIT_FAILURE;
}
