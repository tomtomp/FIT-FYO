/**
 * @file SphericalLense.cpp
 * @author Tomas Polasek
 * @brief Parametrized spherical lense.
 */

#include "SphericalLense.h"

namespace Util
{
    SphericalLense::SphericalLense(float foc1, float radius1, float foc2, float radius2, float thickness, std::size_t rings, std::size_t sectors) :
        mModel(foc1, radius1, foc2, radius2, thickness, rings, sectors)
    { }

    SphericalLense::SphericalLense(const SceneObject &other, float foc1, float radius1, float foc2, float radius2, float thickness, std::size_t rings, std::size_t sectors) :
        Lense(other),
        mModel(foc1, radius1, foc2, radius2, thickness, rings, sectors)
    { }

    void SphericalLense::create()
    {
        mModel.create();
        mCreated = true;
    }

    void SphericalLense::render()
    {
        mModel.render();
    }

    void SphericalLense::destroy()
    {
        mCreated = false;
        mModel.destroy();
    }
}
