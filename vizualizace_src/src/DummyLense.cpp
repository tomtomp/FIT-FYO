/**
 * @file DummyLense.h
 * @author Tomas Polasek
 * @brief This lense is just a dummy used for debugging purposes.
 */

#include "DummyLense.h"

namespace Util
{
    void DummyLense::create()
    {
        mCube.create();
        mCreated = true;
    }

    void DummyLense::render()
    {
        mCube.render();
    }

    void DummyLense::destroy()
    {
        mCreated = false;
        mCube.destroy();
    }
}
