/**
 * @file MainWindow.cpp
 * @author Tomas Polasek
 * @brief Main window of the application.
 */

#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), mUi(new Ui::MainWindow)
{
    mUi->setupUi(this);
    mUi->mainToolBar->hide();
    mUi->renderArea->startRenderer();

    connect(mUi->renderArea, &RenderArea::engineInitialized, this, &MainWindow::engineInitialized);

    mWebcamSrc = new Util::WebcamSrc(mUi->renderArea, this);
    connect(mWebcamSrc, &Util::WebcamSrc::cameraStateChanged, this, &MainWindow::cameraStateChanged);
    connect(mWebcamSrc, &Util::WebcamSrc::readyForCaptureChanged, this, &MainWindow::readyForCaptureChanged);
    mWebcamSrc->setDefaultCamera();

    setupMenuActions();
    setupImageTab();
    setupCameraTab();
    setupLenseTab();
    setupEnvironmentTab();
    setupWebcamTab();
}

MainWindow::~MainWindow()
{
    delete mUi;
}

void MainWindow::setupMenuActions()
{
    connect(mUi->actionLoadImage, &QAction::triggered, this, &MainWindow::loadImage);
    connect(mUi->actionSaveImage, &QAction::triggered, this, &MainWindow::saveImage);
    connect(mUi->actionResetImage, &QAction::triggered, this, &MainWindow::resetImage);
    connect(mUi->actionQuit, &QAction::triggered, this, &MainWindow::quit);
}

void MainWindow::setupImageTab()
{
    auto valueChangedSB = QOverload<double>::of(&QDoubleSpinBox::valueChanged);
    connect(mUi->imageSize, valueChangedSB, this, &MainWindow::setImageSize);

    connect(mUi->imagePositionX, valueChangedSB, this, &MainWindow::setImagePosition);
    connect(mUi->imagePositionY, valueChangedSB, this, &MainWindow::setImagePosition);
    connect(mUi->imagePositionZ, valueChangedSB, this, &MainWindow::setImagePosition);

    auto valueChangedSl = QOverload<int>::of(&QSlider::valueChanged);
    connect(mUi->imageRotationX, valueChangedSl, this, &MainWindow::setImageRotation);
    connect(mUi->imageRotationY, valueChangedSl, this, &MainWindow::setImageRotation);
    connect(mUi->imageRotationZ, valueChangedSl, this, &MainWindow::setImageRotation);
}

void MainWindow::setupCameraTab()
{
    auto valueChangedSB = QOverload<double>::of(&QDoubleSpinBox::valueChanged);
    connect(mUi->cameraPositionX, valueChangedSB, this, &MainWindow::setCameraPosition);
    connect(mUi->cameraPositionY, valueChangedSB, this, &MainWindow::setCameraPosition);
    connect(mUi->cameraPositionZ, valueChangedSB, this, &MainWindow::setCameraPosition);

    connect(mUi->cameraProjectionPerspective, &QRadioButton::toggled, this, &MainWindow::setCameraMode);
    connect(mUi->cameraProjectionPerspectiveFOV, valueChangedSB, this, &MainWindow::setCameraFOV);
    connect(mUi->cameraProjectionOrthographic, &QRadioButton::toggled, this, &MainWindow::setCameraMode);

    // Disable it, since it doesn't work yet...
    mUi->cameraProjectionOrthographic->setEnabled(false);
}

void MainWindow::setupLenseTab()
{
    auto valueChangedSB = QOverload<double>::of(&QDoubleSpinBox::valueChanged);
    connect(mUi->lenseSize, valueChangedSB, this, &MainWindow::setLenseSize);

    connect(mUi->lensePositionX, valueChangedSB, this, &MainWindow::setLensePosition);
    connect(mUi->lensePositionY, valueChangedSB, this, &MainWindow::setLensePosition);
    connect(mUi->lensePositionZ, valueChangedSB, this, &MainWindow::setLensePosition);

    auto valueChangedSl = QOverload<int>::of(&QSlider::valueChanged);
    connect(mUi->lenseRotationX, valueChangedSl, this, &MainWindow::setLenseRotation);
    connect(mUi->lenseRotationY, valueChangedSl, this, &MainWindow::setLenseRotation);
    connect(mUi->lenseRotationZ, valueChangedSl, this, &MainWindow::setLenseRotation);

    connect(mUi->lenseRefractionIndex, valueChangedSB, this, &MainWindow::setLenseRefrIndex);

    connect(mUi->lenseChromaticAberration, &QGroupBox::toggled, this, &MainWindow::setLenseCA);
    connect(mUi->lenseCAAbbe, valueChangedSB, this, &MainWindow::setLenseCAAbbe);

    connect(mUi->lenseShapeCube, &QRadioButton::toggled, this, &MainWindow::setLenseShape);
    connect(mUi->lenseShapeSphere, &QRadioButton::toggled, this, &MainWindow::setLenseShape);
    connect(mUi->lenseShapeModel, &QRadioButton::toggled, this, &MainWindow::setLenseShape);

    connect(mUi->lenseSphFoc1, valueChangedSB, this, &MainWindow::setSphLenseParamsD);
    connect(mUi->lenseSphRad1, valueChangedSB, this, &MainWindow::setSphLenseParamsD);
    connect(mUi->lenseSphFoc2, valueChangedSB, this, &MainWindow::setSphLenseParamsD);
    connect(mUi->lenseSphRad2, valueChangedSB, this, &MainWindow::setSphLenseParamsD);
    connect(mUi->lenseSphThick, valueChangedSB, this, &MainWindow::setSphLenseParamsD);
    auto valueChangedISB = QOverload<int>::of(&QSpinBox::valueChanged);
    connect(mUi->lenseSphRings, valueChangedISB, this, &MainWindow::setSphLenseParamsI);
    connect(mUi->lenseSphSectors, valueChangedISB, this, &MainWindow::setSphLenseParamsI);
}

void MainWindow::setupEnvironmentTab()
{
    auto valueChangedSB = QOverload<double>::of(&QDoubleSpinBox::valueChanged);
    connect(mUi->envRefractionIndex, valueChangedSB, this, &MainWindow::setEnvRefrIndex);
}

void MainWindow::setupWebcamTab()
{
    connect(mUi->webcamStart, &QPushButton::clicked, this, &MainWindow::startCamera);
    connect(mUi->webcamStop, &QPushButton::clicked, this, &MainWindow::stopCamera);
    connect(mUi->webcamCapture, &QPushButton::clicked, this, &MainWindow::startCapture);
    connect(mUi->webcamSnapshot, &QPushButton::clicked, this, &MainWindow::takeSnapshot);
}

void MainWindow::setupEngineConnection()
{
    connect(mUi->renderArea->engine(), &Engine::LenseEngine::rotationChanged, this, &MainWindow::engineRotationChanged);
    connect(mUi->renderArea->engine(), &Engine::LenseEngine::positionChanged, this, &MainWindow::enginePositionChanged);
}

QStringList MainWindow::chooseImage(QFileDialog::AcceptMode acceptMode, QFileDialog::FileMode fileMode)
{
    QFileDialog dialog(this, "Image selection");

    const QStringList pictureLocations = QStandardPaths::standardLocations(QStandardPaths::PicturesLocation);
    dialog.setDirectory(pictureLocations.empty() ? QDir::currentPath() : pictureLocations.first());

    QByteArrayList mimeTypes;
    if (acceptMode == QFileDialog::AcceptOpen)
    {
        mimeTypes = QImageReader::supportedMimeTypes();
    }
    else
    {
        mimeTypes = QImageWriter::supportedMimeTypes();
    }
    QStringList mimeFilters;
    for (auto &mime : mimeTypes)
    {
        mimeFilters.append(mime);
    }
    mimeFilters.sort();
    dialog.setMimeTypeFilters(mimeFilters);
    dialog.selectMimeTypeFilter("image/jpeg");
    if (acceptMode == QFileDialog::AcceptSave)
        dialog.setDefaultSuffix("jpg");

    dialog.setAcceptMode(acceptMode);
    dialog.setFileMode(fileMode);

    QStringList selectedFiles;
    if (dialog.exec())
    {
        selectedFiles = dialog.selectedFiles();
    }
    return selectedFiles;
}

QStringList MainWindow::chooseModel()
{
    QFileDialog dialog(this, "Model selection");

    const QStringList modelLocations = QStandardPaths::standardLocations(QStandardPaths::RuntimeLocation);
    dialog.setDirectory(modelLocations.empty() ? QDir::currentPath() : modelLocations.first());
    dialog.selectNameFilter("*.ply");
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    dialog.setFileMode(QFileDialog::ExistingFile);

    QStringList selectedFiles;
    if (dialog.exec())
    {
        selectedFiles = dialog.selectedFiles();
    }
    return selectedFiles;
}

void MainWindow::loadImage()
{
    // Ask user which one should we load.
    QStringList chosenImage = chooseImage(QFileDialog::AcceptOpen, QFileDialog::ExistingFile);

    if (chosenImage.empty())
    { // User didn't choose any image.
        return;
    }

    QImage image;
    QImageReader reader(chosenImage.first());
    if (!reader.read(&image))
    { // We were unable to load chosen image.
        QMessageBox::critical(this, "Unable to load image!",
                              QString("Unable to load chosen image %1")
                              .arg(chosenImage.first()));
        return;
    }

    mUi->renderArea->setImage(std::move(image.mirrored()));
}

void MainWindow::saveImage()
{
    // Ask user where to save the image to.
    QStringList outputPath = chooseImage(QFileDialog::AcceptSave, QFileDialog::AnyFile);

    if (outputPath.empty())
    { // User didn't choose any image.
        return;
    }

    QImage image = mUi->renderArea->grabFramebuffer();
    if (image.isNull())
    { // Unable to get the framebuffer image.
        QMessageBox::critical(this, "Unable to save image!",
                              "Unable to grab image from rendering area!");
        return;
    }

    QImageWriter writer(outputPath.first());
    if (!writer.write(image))
    { // We are unable to save image.
        QMessageBox::critical(this, "Unable to save image!",
                              QString("Unable to save image to chosen file %1")
                              .arg(outputPath.first()));
        return;
    }
}

void MainWindow::resetImage()
{
    mUi->renderArea->setDefaultImage();
}

void MainWindow::quit()
{
    QApplication::quit();
}

void MainWindow::startCamera()
{
    mWebcamSrc->startCamera();
}

void MainWindow::stopCamera()
{
    mWebcamSrc->stopCamera();
}

void MainWindow::startCapture()
{
    mWebcamSrc->startCapture();
    mUi->webcamCapture->setEnabled(false);
}

void MainWindow::takeSnapshot()
{
    mWebcamSrc->captureImage();
    mUi->webcamCapture->setEnabled(true);
}

void MainWindow::engineRotationChanged(const QVector3D &newEulerAngles)
{
    mUi->lenseRotationX->blockSignals(true);
    mUi->lenseRotationY->blockSignals(true);
    mUi->lenseRotationZ->blockSignals(true);
    {
        mUi->lenseRotationX->setValue(newEulerAngles.x());
        mUi->lenseRotationY->setValue(newEulerAngles.y());
        mUi->lenseRotationZ->setValue(newEulerAngles.z());
    }
    mUi->lenseRotationZ->blockSignals(false);
    mUi->lenseRotationY->blockSignals(false);
    mUi->lenseRotationX->blockSignals(false);
}

void MainWindow::enginePositionChanged(const QVector3D &newPosition)
{
    mUi->lensePositionX->blockSignals(true);
    mUi->lensePositionY->blockSignals(true);
    mUi->lensePositionZ->blockSignals(true);
    {
        mUi->lensePositionX->setValue(newPosition.x());
        mUi->lensePositionY->setValue(newPosition.y());
        mUi->lensePositionZ->setValue(newPosition.z());
    }
    mUi->lensePositionZ->blockSignals(false);
    mUi->lensePositionY->blockSignals(false);
    mUi->lensePositionX->blockSignals(false);
}

void MainWindow::engineInitialized()
{
    // Image tab default value initialization:
    mUi->imageSize->setValue(Config::IMAGE_SIZE);

    mUi->imagePositionX->setValue(Config::IMAGE_POS_X);
    mUi->imagePositionY->setValue(Config::IMAGE_POS_Y);
    mUi->imagePositionZ->setValue(Config::IMAGE_POS_Z);

    mUi->imageRotationX->setValue(Config::IMAGE_ROT_X);
    mUi->imageRotationY->setValue(Config::IMAGE_ROT_Y);
    mUi->imageRotationZ->setValue(Config::IMAGE_ROT_Z);

    // Camera tab default value initialization:
    mUi->cameraPositionX->setValue(Config::CAMERA_POS_X);
    mUi->cameraPositionY->setValue(Config::CAMERA_POS_Y);
    mUi->cameraPositionZ->setValue(Config::CAMERA_POS_Z);

    mUi->cameraProjectionPerspectiveFOV->setValue(Config::CAMERA_FOV);
    if (Config::CAMERA_PERSPECTIVE)
    {
        mUi->cameraProjectionPerspective->setChecked(true);
    }
    else
    {
        mUi->cameraProjectionOrthographic->setChecked(true);
    }

    // Lense tab default value initialization:
    mUi->lenseSize->setValue(Config::LENSE_SIZE);

    mUi->lensePositionX->setValue(Config::LENSE_POS_X);
    mUi->lensePositionY->setValue(Config::LENSE_POS_Y);
    mUi->lensePositionZ->setValue(Config::LENSE_POS_Z);

    mUi->lenseRotationX->setValue(Config::LENSE_ROT_X);
    mUi->lenseRotationY->setValue(Config::LENSE_ROT_Y);
    mUi->lenseRotationZ->setValue(Config::LENSE_ROT_Z);

    mUi->lenseRefractionIndex->setValue(Config::LENSE_REFR_INDEX);
    mUi->lenseCAAbbe->setValue(Config::LENSE_ABBE_NUMBER);
    mUi->lenseChromaticAberration->setChecked(Config::LENSE_CHROMATIC_ABERRATION);

    // Environment tab default value initialization:
    mUi->envRefractionIndex->setValue(Config::ENV_REFR_INDEX);

    if (Config::LENSE_CUBE_ENABLED)
    {
        mUi->lenseSphParamBox->setEnabled(false);
        mUi->lenseShapeCube->setChecked(true);
    }
    else
    {
        mUi->lenseSphParamBox->setEnabled(true);
        mUi->lenseShapeSphere->setChecked(true);
    }

    mUi->lenseSphFoc1->setValue(Config::LENSE_SPHERE_FOC1);
    mUi->lenseSphRad1->setValue(Config::LENSE_SPHERE_RAD1);
    mUi->lenseSphFoc2->setValue(Config::LENSE_SPHERE_FOC2);
    mUi->lenseSphRad2->setValue(Config::LENSE_SPHERE_RAD2);
    mUi->lenseSphThick->setValue(Config::LENSE_SPHERE_THICK);
    mUi->lenseSphRings->setValue(Config::LENSE_SPHERE_RINGS);
    mUi->lenseSphSectors->setValue(Config::LENSE_SPHERE_SECTORS);

    setupEngineConnection();
}

void MainWindow::setImageSize(double size)
{
    mUi->renderArea->setImageSize(size);
}

void MainWindow::setImageRotation(int)
{
    mUi->renderArea->setImageRotation(mUi->imageRotationX->value(),
                                      mUi->imageRotationY->value(),
                                      mUi->imageRotationZ->value());
}

void MainWindow::setImagePosition(double)
{
    mUi->renderArea->setImagePosition(mUi->imagePositionX->value(),
                                      mUi->imagePositionY->value(),
                                      mUi->imagePositionZ->value());
}

void MainWindow::setCameraPosition(double)
{
    mUi->renderArea->setCameraPosition(mUi->cameraPositionX->value(),
                                       mUi->cameraPositionY->value(),
                                       mUi->cameraPositionZ->value());
}

void MainWindow::setCameraMode(bool checked)
{
    if (checked)
    { // Check flag, so we change the mode only once.
        if (mUi->cameraProjectionPerspective->isChecked())
        {
            mUi->cameraProjectionPerspectiveFOV->setEnabled(true);
            mUi->renderArea->setCameraProjection(mUi->cameraProjectionPerspectiveFOV->value());
        }
        else if (mUi->cameraProjectionOrthographic->isChecked())
        {
            mUi->cameraProjectionPerspectiveFOV->setEnabled(false);
            mUi->renderArea->setCameraOrthographic();
        }
    }
}

void MainWindow::setCameraFOV(double fov)
{
    mUi->renderArea->setCameraProjection(fov);
}

void MainWindow::setLenseSize(double size)
{
    mUi->renderArea->setLenseSize(size);
}

void MainWindow::setLensePosition(double)
{
    mUi->renderArea->setLensePosition(mUi->lensePositionX->value(),
                                      mUi->lensePositionY->value(),
                                      mUi->lensePositionZ->value());
}

void MainWindow::setLenseRotation(double)
{
    mUi->renderArea->setLenseRotation(mUi->lenseRotationX->value(),
                                      mUi->lenseRotationY->value(),
                                      mUi->lenseRotationZ->value());
}

void MainWindow::setLenseRefrIndex(double newIndex)
{
    mUi->renderArea->setLenseRefrIndex(newIndex);
}

void MainWindow::setLenseCA(bool checked)
{
    mUi->renderArea->setLenseCA(checked, mUi->lenseCAAbbe->value());
}

void MainWindow::setLenseCAAbbe(double abbe)
{
    mUi->renderArea->setLenseCA(mUi->lenseChromaticAberration->isChecked(), abbe);
}

void MainWindow::setLenseShape(bool checked)
{
    if (checked)
    { // Check, to only change it once.
        if (mUi->lenseShapeSphere->isChecked())
        {
            mUi->lenseSphParamBox->setEnabled(true);
            setSphLenseParamVals();
        }
        else if (mUi->lenseShapeCube->isChecked())
        {
            mUi->lenseSphParamBox->setEnabled(false);
            mUi->renderArea->setLenseTypeCube();
        }
        else if (mUi->lenseShapeModel->isChecked())
        {
            mUi->lenseSphParamBox->setEnabled(false);
            loadModel();
        }
    }
}

void MainWindow::setSphLenseParamsD(double)
{
    if (mUi->lenseShapeSphere->isChecked())
    {
        setSphLenseParamVals();
    }
}

void MainWindow::setSphLenseParamsI(int)
{
    if (mUi->lenseShapeSphere->isChecked())
    {
        setSphLenseParamVals();
    }
}

void MainWindow::setSphLenseParamVals()
{
    mUi->renderArea->setLenseTypeSphere(mUi->lenseSphFoc1->value(), mUi->lenseSphRad1->value(),
                                        mUi->lenseSphFoc2->value(), mUi->lenseSphRad2->value(),
                                        mUi->lenseSphThick->value(),
                                        mUi->lenseSphRings->value(), mUi->lenseSphSectors->value());
}

void MainWindow::loadModel()
{
    // Ask user which one should we load.
    QStringList chosenModel = chooseModel();

    if (chosenModel.empty())
    { // User didn't choose any image.
        mUi->lenseShapeCube->setChecked(true);
        return;
    }

    QFile input(chosenModel.first());
    try {
        mUi->renderArea->setLenseModel(input);
    } catch (const std::runtime_error &err) {
        QMessageBox::critical(this, "Unable to load model!",
                              QString("Unable to load chosen model %1 : %2")
                              .arg(chosenModel.first(), err.what()));
        mUi->lenseShapeCube->setChecked(true);
    }
}

void MainWindow::setEnvRefrIndex(double newIndex)
{
    mUi->renderArea->setEnvRefrIndex(newIndex);
}

void MainWindow::cameraStateChanged(QCamera::State state)
{
    switch (state)
    {
        case QCamera::ActiveState:
        { // Camera is running.
            mUi->webcamStart->setEnabled(false);
            mUi->webcamStop->setEnabled(true);
            mUi->webcamCapture->setEnabled(false);
            mUi->webcamSnapshot->setEnabled(mWebcamSrc->supportsImageCapture());
            break;
        }
        case QCamera::UnloadedState:
        case QCamera::LoadedState:
        { // Camera is not running.
            mUi->webcamStart->setEnabled(true);
            mUi->webcamStop->setEnabled(false);
            mUi->webcamCapture->setEnabled(false);
            mUi->webcamSnapshot->setEnabled(false);
            break;
        }
    }
}

void MainWindow::readyForCaptureChanged(bool ready)
{
    mUi->webcamSnapshot->setEnabled(ready);
}
