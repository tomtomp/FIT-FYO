/**
 * @file ImageSrc.cpp
 * @author Tomas Polasek
 * @brief Source image being displayed in the OpenGL.
 */

#include "ImageSrc.h"

namespace Util
{
    ImageSrc::ImageSrc(bool loadDefault) :
        mUpload{false}, mTextureUnit{0u}, mTexture(QOpenGLTexture::Target2D)
    {
        if (loadDefault)
        {
            loadDefaultImage();
        }

        setDefaultPosition();
    }

    ImageSrc::~ImageSrc()
    {
        mUpload = false;
    }

    void ImageSrc::setImage(QImage &&img, bool destroy)
    {
        //qDebug() << "Setting image: " << img.format() << " : " << img.width() << " x " << img.height();
        mImage = std::move(img);
        mFormatChanged = destroy;
        mUpload = true;
    }

    void ImageSrc::setDefaultImage()
    {
        // TODO - Do this only once?
        loadDefaultImage();
    }

    void ImageSrc::create()
    {
        mPlane.create();
        if (mImage.isNull())
        {
            loadDefaultImage();
        }
        checkUpload();

        mCreated = true;
    }

    void ImageSrc::destroy()
    {
        mTexture.destroy();
        mPlane.destroy();
    }

    void ImageSrc::setScale(float scale)
    {
        float widthToHeight{static_cast<float>(width()) / height()};
        QVector3D perspectiveScale{scale * widthToHeight, scale, scale};
        SceneObject::setScale(perspectiveScale);
    }

    void ImageSrc::render()
    {
        bind();
        mPlane.render();
        release();
    }

    void ImageSrc::bind()
    {
        // Check if the texture is uploaded or upload it.
        checkUpload();

        mTexture.bind(mTextureUnit);
    }

    void ImageSrc::release()
    {
        mTexture.release();
    }

    void ImageSrc::loadDefaultImage()
    {
        // Create the new image.
        mImage = QImage(DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT, DEFAULT_IMAGE_FORMAT);
        QRgb *pixels = reinterpret_cast<QRgb*>(mImage.bits());

        // Fill image with testing checkerboard pattern.
        for (std::size_t y = 0; y < DEFAULT_IMAGE_HEIGHT; ++y)
        {
            for (std::size_t x = 0; x < DEFAULT_IMAGE_WIDTH; ++x)
            {
                QRgb &pixel = pixels[y * DEFAULT_IMAGE_WIDTH + x];
                int intensity = 255 * (((x >> DEFAULT_IMAGE_CHECKER_MULTIPLE)
                                        + (y >> DEFAULT_IMAGE_CHECKER_MULTIPLE)) & 1);
                pixel = qRgba(intensity, intensity, intensity, 255);
            }
        }

        /*
        QImageWriter writer("test.jpg");
        writer.write(mImage);
        */

        mFormatChanged = true;
        mUpload = true;
    }

    void ImageSrc::setDefaultPosition()
    {
        setPosition({0.0f, 0.0f, 0.0f});
        setEulerRotation({0.0f, 0.0f, 0.0f});
        setScale(1.0f);
    }

    void ImageSrc::checkUpload()
    {
        if (mUpload || !mTexture.isCreated())
        {
            if (mFormatChanged)
            {
                mTexture.destroy();
                mFormatChanged = false;
            }
            //mTexture.setData(mImage, QOpenGLTexture::DontGenerateMipMaps);
            mTexture.setData(mImage, QOpenGLTexture::GenerateMipMaps);
            //mTexture.setMinMagFilters(QOpenGLTexture::LinearMipMapLinear, QOpenGLTexture::LinearMipMapLinear);
            mTexture.setWrapMode(QOpenGLTexture::ClampToBorder);

            // Re-calculate the scale.
            setScale(scale().y());

            mUpload = false;
        }
    }
}
