/**
 * @file WebcamSrc.cpp
 * @author Tomas Polasek
 * @brief Class acting as interface between the application and Qt Camera.
 */

#include "WebcamSrc.h"

namespace Util
{
    FakeVideoSurface::FakeVideoSurface(WebcamSrc *parent) :
        QAbstractVideoSurface(parent), mDest{parent}
    {
        Q_ASSERT(parent != nullptr);
    }

    FakeVideoSurface::~FakeVideoSurface()
    {

    }

    QList<QVideoFrame::PixelFormat>
    FakeVideoSurface::supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType) const
    {
        Q_UNUSED(handleType)

        QList<QVideoFrame::PixelFormat> supportedFormats;

        supportedFormats << QVideoFrame::Format_RGB24 << QVideoFrame::Format_ARGB32;

        return supportedFormats;
    }

    bool FakeVideoSurface::start(const QVideoSurfaceFormat &format)
    {
        //qDebug() << "Starting capture" << format;
        mPresent = true;
        return QAbstractVideoSurface::start(format);
    }

    void FakeVideoSurface::stop()
    {
        //qDebug() << "Stopping capture";
        mPresent = false;
        QAbstractVideoSurface::stop();
    }

    bool FakeVideoSurface::present(const QVideoFrame &frame)
    {
        bool presented{!mPresent};
        if (frame.isValid() && isActive() && mPresent)
        {
            QVideoFrame videoFrame(frame);
            if (videoFrame.map(QAbstractVideoBuffer::ReadOnly))
            {
                // Copy the frame data.
                QImage image = QImage(videoFrame.bits(),
                             videoFrame.width(),
                             videoFrame.height(),
                             QVideoFrame::imageFormatFromPixelFormat(videoFrame.pixelFormat())).mirrored().copy();

                mDest->presentVideo(std::move(image));
                presented = true;

            }
            videoFrame.unmap();
        }

        return presented;
    }

    WebcamSrc::WebcamSrc(RenderArea *renderArea, QObject *parent) :
        QObject(parent), mRenderArea{renderArea},
        mFakeSurface{new FakeVideoSurface(this)}
    {
        Q_ASSERT(renderArea);
    }

    WebcamSrc::~WebcamSrc()
    {

    }

    const QList<QCameraInfo> WebcamSrc::availableCameras() const
    {
        return QCameraInfo::availableCameras();
    }

    void WebcamSrc::setCamera(const QCameraInfo &info)
    {
        useCamera(info);
    }

    void WebcamSrc::setDefaultCamera()
    {
        useCamera(QCameraInfo::defaultCamera());
    }

    void WebcamSrc::startCamera()
    {
        mCamera->load();
        mCamera->setCaptureMode(QCamera::CaptureStillImage);
        mCamera->start();
        mFakeSurface->setPresent(true);
        emit cameraStateChanged(mCamera->state());
    }

    void WebcamSrc::stopCamera()
    {
        //mCamera->setCaptureMode(QCamera::CaptureViewfinder);
        mCamera->unlock();
        mCamera->stop();
        mCamera->unload();
        emit cameraStateChanged(mCamera->state());
    }

    void WebcamSrc::startCapture()
    {
        mVideoFormatChanged = true;
        //mCamera->setCaptureMode(QCamera::CaptureStillImage);
        mFakeSurface->setPresent(true);
    }

    void WebcamSrc::stopCapture()
    {
        //mCamera->setViewfinder((QAbstractVideoSurface*)nullptr);
        //mCamera->setCaptureMode(QCamera::CaptureStillImage);
        mVideoFormatChanged = true;
    }

    void WebcamSrc::captureImage()
    {
        mVideoFormatChanged = true;
        //mCamera->setCaptureMode(QCamera::CaptureStillImage);
        mFakeSurface->setPresent(false);
        mCamera->searchAndLock();
        mImageCapture->capture();
        mCamera->unlock();
    }

    void WebcamSrc::presentImage(QImage &&image)
    {
        mRenderArea->setImage(std::move(image));
    }

    void WebcamSrc::presentVideo(QImage &&frame)
    {
        // TODO - Fix, we shouldn't need to re-create each frame!
        mVideoFormatChanged = true;
        mRenderArea->setVideoFrame(std::move(frame), mVideoFormatChanged);
        mVideoFormatChanged = false;
    }

    bool WebcamSrc::supportsVideoCapture() const
    {
        return mCamera->isCaptureModeSupported(QCamera::CaptureVideo);
    }

    bool WebcamSrc::supportsImageCapture() const
    {
        return mCamera->isCaptureModeSupported(QCamera::CaptureStillImage);
    }

    void WebcamSrc::useCamera(const QCameraInfo &info)
    {
        mCamera.reset(new QCamera(info));
        connect(mCamera.data(), &QCamera::stateChanged, this, &WebcamSrc::cameraStateChanged);
        connect(mCamera.data(), QOverload<QCamera::Error>::of(&QCamera::error), this, &WebcamSrc::displayCameraError);

        mImageCapture.reset(new QCameraImageCapture(mCamera.data()));
        mImageCapture->setCaptureDestination(QCameraImageCapture::CaptureToBuffer);
        mImageCapture->setBufferFormat(QVideoFrame::Format_RGB24);
        connect(mImageCapture.data(), &QCameraImageCapture::readyForCaptureChanged, this, &WebcamSrc::readyForCaptureChanged);
        // TODO - Fix saving to disk!
        // Fixed?
        //connect(mImageCapture.data(), &QCameraImageCapture::imageCaptured, this, &WebcamSrc::imageCaptured);
        connect(mImageCapture.data(), &QCameraImageCapture::imageAvailable, this, &WebcamSrc::imageAvailable);
        connect(mImageCapture.data(), QOverload<int, QCameraImageCapture::Error, const QString &>::of(&QCameraImageCapture::error),
                this, &WebcamSrc::displayImageError);

        mCamera->setViewfinder(mFakeSurface);
        //mCamera->setCaptureMode(QCamera::CaptureStillImage);

        emit cameraStateChanged(mCamera->state());
    }

    void WebcamSrc::displayCameraError() const
    {
        QMessageBox::warning(mRenderArea, "Camera Error", mCamera->errorString());
    }

    void WebcamSrc::displayImageError(int id, const QCameraImageCapture::Error error, const QString &errorString) const
    {
        Q_UNUSED(id);
        Q_UNUSED(error);

        QMessageBox::warning(mRenderArea, "Image capture Error", errorString);
    }

    void WebcamSrc::imageCaptured(int reqId, const QImage &image)
    {
        Q_UNUSED(reqId);
        Q_UNUSED(image);

        //QImage cloneImage(image);
        //mRenderArea->setImage(std::move(cloneImage));
    }

    void WebcamSrc::imageAvailable(int reqId, const QVideoFrame &frame)
    {
        Q_UNUSED(reqId);

        if (frame.isValid())
        {
            QVideoFrame videoFrame(frame);
            if (videoFrame.map(QAbstractVideoBuffer::ReadOnly))
            {
                //qDebug() << videoFrame.width() << " " << videoFrame.height();
                // Copy the frame data.
                QImage image = QImage(videoFrame.bits(),
                             videoFrame.width(),
                             videoFrame.height(),
                             QVideoFrame::imageFormatFromPixelFormat(videoFrame.pixelFormat())).mirrored().copy();

                //qDebug() << videoFrame.bits() << " " << image.isNull();

                mRenderArea->setImage(std::move(image));

            }
            videoFrame.unmap();
        }
    }
}
