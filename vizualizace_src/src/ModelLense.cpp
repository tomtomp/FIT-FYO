/**
 * @file ModelLense.cpp
 * @author Tomas Polasek
 * @brief Lense using model from a file.
 */

#include "ModelLense.h"

namespace Util
{
    ModelLense::ModelLense(QFile &input) :
        mModel(input)
    { }

    ModelLense::ModelLense(const SceneObject &other, QFile &input) :
        Lense(other),
        mModel(input)
    { }

    void ModelLense::create()
    {
        qDebug() << "Creating model lense...";
        mModel.create();
        mCreated = true;
    }

    void ModelLense::render()
    {
        mModel.render();
    }

    void ModelLense::destroy()
    {
        mCreated = false;
        mModel.destroy();
    }
}
