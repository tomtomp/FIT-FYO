/**
 * @file ModelMesh.cpp
 * @author Tomas Polasek
 * @brief Model mesh loadable from a file.
 */

#include "ModelMesh.h"

namespace Util
{
    ModelMesh::ModelMesh(QFile &input)
    {
        loadModel(input);
    }

    void ModelMesh::loadModel(QFile &input)
    {
        QByteArray line;

        if (!input.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            throw std::runtime_error(std::string("Error, cannot open mesh resource. "));
        }

        line = input.readLine();

        while (!strStartsWith(line.toStdString(), "element vertex ") && !input.atEnd())
        {
            line = input.readLine();
        }

        if (input.atEnd())
        {
            throw std::runtime_error(std::string("Not enough data in mesh file. "));
        }

        QString vertexNumStr(line.cbegin() + std::strlen("element vertex "));
        bool conversionOk{false};
        int vertexNum{vertexNumStr.toInt(&conversionOk)};
        if (!conversionOk)
        {
            throw std::runtime_error(std::string("Cannot convert number of vertexes to number. "));
        }

        while (!strStartsWith(line.toStdString(), "element face ") && !input.atEnd())
        {
            line = input.readLine();
        }

        if (input.atEnd())
        {
            throw std::runtime_error(std::string("Not enough data in mesh file. "));
        }

        QString faceNumStr(line.cbegin() + std::strlen("element face "));
        int faceNum{faceNumStr.toInt(&conversionOk)};
        if (!conversionOk)
        {
            throw std::runtime_error(std::string("Cannot convert number of faces to number. "));
        }

        while (line != "end_header\n" && !input.atEnd())
        {
            line = input.readLine();
        }

        if (input.atEnd())
        {
            throw std::runtime_error(std::string("Not enough data in mesh file. "));
        }

        int readVertexes;
        for (readVertexes = 0; readVertexes < vertexNum && !input.atEnd(); ++readVertexes)
        {
            line = input.readLine();
            mVertexData.push_back(parseLine(line));
        }

        if (readVertexes != vertexNum)
        {
            throw std::runtime_error(std::string("Couldn't read all the vertexes. "));
        }

        int readFaces;
        for (readFaces = 0; readFaces < faceNum && !input.atEnd(); ++readFaces)
        {
            line = input.readLine();
            addFaces(line);
        }

        if (readFaces != faceNum)
        {
            throw std::runtime_error(std::string("Couldn't read all the faces. "));
        }
    }

    ModelMesh::VertexView ModelMesh::parseLine(const QByteArray &line)
    {
        QTextStream input(line);

        float pX{0}, pY{0}, pZ{0};
        float nX{0}, nY{0}, nZ{0};
        float tU{0}, tV{0};

        input >> pX >> pY >> pZ >> nX >> nY >> nZ >> tU >> tV;

        return VertexView{{pX, pY, pZ}, {nX, nY, nZ}, {tU, tV}};
    }

    void ModelMesh::addFaces(const QByteArray &line)
    {
        QTextStream input(line);

        int indexNum{0};
        std::vector<unsigned int> indices;
        unsigned int index;

        input >> indexNum;

        if (indexNum != 3 && indexNum != 4)
        {
            throw std::runtime_error(std::string("Only 3 and 4 indices are supported."));
        }

        int numRead;
        for (numRead = 0; numRead < indexNum; ++numRead)
        {
            input >> index;
            indices.push_back(index);
        }

        if (numRead != indexNum)
        {
            throw std::runtime_error(std::string("Not all indices could be read."));
        }

        // 1->2->3
        mIndexData.push_back(indices[0]);
        mIndexData.push_back(indices[1]);
        mIndexData.push_back(indices[2]);

        if (indexNum == 4)
        {
            // 1->3->4
            mIndexData.push_back(indices[0]);
            mIndexData.push_back(indices[2]);
            mIndexData.push_back(indices[3]);
        }
    }

    bool ModelMesh::strStartsWith(const std::string &str, const std::string &search)
    {
        if (str.length() >= search.length())
        {
            return str.compare(0, search.length(), search) == 0;
        }
        else
        {
            return false;
        }
    }

    void ModelMesh::create()
    {
        // Create vertex array.
        gl.glGenVertexArrays(1, &mVa);
        if (!mVa)
        {
            throw std::runtime_error("Unable to glGenVertexArrays!");
        }
        gl.glBindVertexArray(mVa);

        // Create vertex buffer object.
        gl.glGenBuffers(1, &mVb);
        if (!mVb)
        {
            destroy();
            throw std::runtime_error("Unable to glGenBuffers!");
        }
        gl.glBindBuffer(GL_ARRAY_BUFFER, mVb);

        // Set the data.
        qDebug() << "Vertex buffer size: " << mVertexData.size();
        qDebug() << "First: " << mVertexData[0].pos[0] << " " << mVertexData[0].pos[1] << " " << mVertexData[0].pos[2];
        gl.glBufferData(GL_ARRAY_BUFFER, mVertexData.size() * sizeof(mVertexData[0]), mVertexData.data(), GL_STATIC_DRAW);

        // Create index buffer object.
        gl.glGenBuffers(1, &mEb);
        if (!mEb)
        {
            destroy();
            throw std::runtime_error("Unable to glGenBuffers!");
        }
        gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEb);

        // Set the data.
        qDebug() << "Index buffer size: " << mIndexData.size();
        qDebug() << "First: " << mIndexData[0] << " " << mIndexData[1] << " " << mIndexData[2];
        gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndexData.size() * sizeof(mIndexData[0]), mIndexData.data(), GL_STATIC_DRAW);

        // Setup vertex puller.
        gl.glVertexAttribPointer(ATTRIBUTE_LOC_POS, 3, GL_FLOAT, GL_FALSE,
                                 sizeof(GLfloat) * COMP_PER_VERTEX,
                                 reinterpret_cast<void*>(sizeof(GLfloat) * 0));
        gl.glEnableVertexAttribArray(ATTRIBUTE_LOC_POS);
        gl.glVertexAttribPointer(ATTRIBUTE_LOC_NORM, 3, GL_FLOAT, GL_FALSE,
                                 sizeof(GLfloat) * COMP_PER_VERTEX,
                                 reinterpret_cast<void*>(sizeof(GLfloat) * 3));
        gl.glEnableVertexAttribArray(ATTRIBUTE_LOC_NORM);
        gl.glVertexAttribPointer(ATTRIBUTE_LOC_UV, 2, GL_FLOAT, GL_FALSE,
                                 sizeof(GLfloat) * COMP_PER_VERTEX,
                                 reinterpret_cast<void*>(sizeof(GLfloat) * 6));
        gl.glEnableVertexAttribArray(ATTRIBUTE_LOC_UV);

        // Unbind the buffers and array.
        gl.glBindBuffer(GL_ARRAY_BUFFER, 0);
        gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        gl.glBindVertexArray(0);

        // We can delete the buffers since VAO is holding a reference to it.
        //gl.glDeleteBuffers(1, &mEb);
        //mEb = 0u;
        gl.glDeleteBuffers(1, &mVb);
        mVb = 0u;

        mCreated = true;
    }

    void ModelMesh::render()
    {
        // Bind the vertex array.
        gl.glBindVertexArray(mVa);

        gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEb);

        // Draw the quads.
        gl.glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(mIndexData.size()), GL_UNSIGNED_INT, nullptr);

        // Disable the vertex array.
        gl.glBindVertexArray(0);
    }

    void ModelMesh::destroy()
    {
        mCreated = false;

        if (mEb)
        {
            gl.glDeleteBuffers(1, &mEb);
            mEb = 0;
        }

        if (mVa)
        {
            gl.glDeleteVertexArrays(1, &mVa);
            mVa = 0;
        }

        if (mVb)
        {
            gl.glDeleteBuffers(1, &mVb);
            mVb = 0;
        }
    }
}
