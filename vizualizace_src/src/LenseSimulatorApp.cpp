/**
 * @file LenseSimulatorApp.cpp
 * @author Tomas Polasek
 * @brief Main application class.
 */

#include "LenseSimulatorApp.h"

LenseSimulatorApp::LenseSimulatorApp(int &argc, char **argv) :
    QApplication(argc, argv)
{
    mWindow.show();
}

LenseSimulatorApp::~LenseSimulatorApp()
{

}

int LenseSimulatorApp::start()
{
    return exec();
}
