/**
 * @file Plane.cpp
 * @author Tomas Polasek
 * @brief Simple plane renderable in OpenGL.
 */

#include "Plane.h"

namespace Util
{
    const GLfloat Plane::VERTEX_BUFFER_DATA[] =
        {
        // x      y     z    nx    ny    nz     u     v
        -1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
         1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

        -1.0f,  1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
         1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
         1.0f,  1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
        };

    Plane::Plane() :
        mVa{0u}, mVb{0u}
    { }

    void Plane::create()
    {
        // Create vertex array.
        gl.glGenVertexArrays(1, &mVa);
        if (!mVa)
        {
            throw std::runtime_error("Unable to glGenVertexArrays!");
        }
        gl.glBindVertexArray(mVa);

        // Create vertex buffer object.
        gl.glGenBuffers(1, &mVb);
        if (!mVb)
        {
            destroy();
            throw std::runtime_error("Unable to glGenBuffers!");
        }
        gl.glBindBuffer(GL_ARRAY_BUFFER, mVb);

        // Set the data.
        gl.glBufferData(GL_ARRAY_BUFFER, sizeof(VERTEX_BUFFER_DATA), VERTEX_BUFFER_DATA, GL_STATIC_DRAW);

        // Setup vertex puller.
        gl.glVertexAttribPointer(ATTRIBUTE_LOC_POS, 3, GL_FLOAT, GL_FALSE,
                                 sizeof(GLfloat) * VALS_PER_VERTEX,
                                 reinterpret_cast<void*>(sizeof(GLfloat) * 0));
        gl.glEnableVertexAttribArray(ATTRIBUTE_LOC_POS);
        gl.glVertexAttribPointer(ATTRIBUTE_LOC_NORM, 3, GL_FLOAT, GL_FALSE,
                                 sizeof(GLfloat) * VALS_PER_VERTEX,
                                 reinterpret_cast<void*>(sizeof(GLfloat) * 3));
        gl.glEnableVertexAttribArray(ATTRIBUTE_LOC_NORM);
        gl.glVertexAttribPointer(ATTRIBUTE_LOC_UV, 2, GL_FLOAT, GL_FALSE,
                                 sizeof(GLfloat) * VALS_PER_VERTEX,
                                 reinterpret_cast<void*>(sizeof(GLfloat) * 6));
        gl.glEnableVertexAttribArray(ATTRIBUTE_LOC_UV);

        // Unbind the buffer and array.
        gl.glBindBuffer(GL_ARRAY_BUFFER, 0);
        gl.glBindVertexArray(0);

        // We can delete the buffer since VAO is holding a reference to it.
        gl.glDeleteBuffers(1, &mVb);
        mVb = 0u;

        mCreated = true;
    }

    void Plane::render()
    {
        // Bind the vertex array.
        gl.glBindVertexArray(mVa);

        // Draw the triangles.
        gl.glDrawArrays(GL_TRIANGLES, 0, NUM_VERTICES);

        // Disable the vertex array.
        gl.glBindVertexArray(0);
    }

    Plane::~Plane()
    { destroy(); }

    void Plane::destroy()
    {
        if (mVb)
        {
            gl.glDeleteBuffers(1, &mVb);
            mVb = 0u;
        }

        if (mVa)
        {
            gl.glDeleteVertexArrays(1, &mVa);
            mVa = 0u;
        }

        mCreated = false;
    }
}
