#version 450 core

uniform mat4x4 mv;
uniform mat4x4 mvp;
uniform mat3x3 invmt;

layout(location = 0) in vec3 vertPos;
layout(location = 1) in vec3 vertNorm;
layout(location = 2) in vec2 vertUv;

out float fragDist;
out vec3 fragPos;
out vec3 fragNorm;
out vec2 fragUv;

out float fragRatio;
out vec3 fragReflect;
out vec3 fragRefract;

const float ETA = 0.66;
const float FRESNEL_POWER = 5.0;
const float F = ((1.0 - ETA) * (1.0 - ETA)) / ((1.0 + ETA) * (1.0 + ETA));

void main()
{
    vec4 ray = mv * vec4(vertPos, 1.0);
    ray.xyz /= ray.w;
    vec3 dir = normalize(ray.xyz / ray.w);

    //fragDist = length(vec3(vec4(mv * vec4(vertPos, 1.0)).xyz));
    fragDist = length(ray.xyz);
    fragPos = ray.xyz;
    fragNorm = normalize(invmt * vertNorm);

    gl_Position = mvp * vec4(vertPos, 1.0);
    fragUv = (gl_Position.xy / gl_Position.w + 1.0f) / 2.0f;

    fragRatio = F + (1.0 - F) * pow((1.0 - dot(-dir, fragNorm)), FRESNEL_POWER);
    fragReflect = reflect(dir, fragNorm);
    fragRefract = refract(dir, fragNorm, ETA);
}
