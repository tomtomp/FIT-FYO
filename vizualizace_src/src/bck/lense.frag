#version 450 core

#define M_PI 3.141592653589793238462643383
#define M_2PI 2.0 * 3.141592653589793238462643383

/// Depth map of the lense.
uniform sampler2D depthNormalMap;
/// Image behind the lense.
uniform sampler2D background;
/// Mode used for debugging.
uniform int mode;
/// Projection matrix.
uniform mat4x4 p;
/// Environment refraction index.
uniform float envRI;
/// Lense refraction index.
uniform float lRI;

const float eta = lRI / envRI;

in float fragDist;
in vec3 fragPos;
in vec3 fragNorm;
noperspective in vec2 fragUv;

in float fragRatio;
in vec3 fragReflect;
in vec3 fragRefract;

out vec4 outColor;

void main()
{
    vec4 backgroundSample = texture2D(background, fragUv);
    vec4 depthNormalSample = texture2D(depthNormalMap, fragUv);
    vec3 otherNorm = depthNormalSample.xyz;
    float otherDist = depthNormalSample.w;

    float lengthInside = abs(otherDist - fragDist);

    vec3 secRefractPos = fragPos + fragRefract * ((fragPos.z - lengthInside) / fragRefract.z);
    vec3 secRefract = refract(fragRefract, fragNorm, eta);
    vec3 res = secRefractPos + secRefract * ((secRefractPos.z - 3.0f) / secRefract.z);
    //res = (res + 1.0f) / 2.0f;
    backgroundSample = texture2D(background, res.xy);

    //vec4 t = p * vec4(secRefractPos, 1.0f);
    //vec4 t = p * vec4(fragPos, 1.0f);
    vec4 t = p * vec4(res, 1.0f);
    t /= t.w;
    t.xyz += 1.0f;
    t.xyz /= 2.0f;
    //backgroundSample = vec4(abs(texture2D(depthNormalMap, t.xy).rgb), 1.0f);
    backgroundSample = vec4(texture2D(background, t.xy).rgb, 1.0f);
    //backgroundSample = vec4(t.xy, 0.0f, 1.0f);

    if (mode == 0)
    {
        outColor = vec4(fragDist / 3.0f, fragDist / 3.0f, fragDist / 3.0f, 1.0f);
    }
    else if (mode == 1)
    {
        outColor = vec4(otherDist / 4.0f, otherDist / 4.0f, otherDist / 4.0f, 1.0f);
    }
    else if (mode == 2)
    {
        outColor = vec4(lengthInside / 2.0f, lengthInside / 2.0f, lengthInside / 2.0f, 1.0f);
    }
    else if (mode == 3)
    {
        outColor = vec4(fragRatio, fragRatio, fragRatio, 1.0f);
    }
    else
    {
        outColor = backgroundSample;
    }

    //outColor = vec4(otherDist, otherDist, otherDist, 1.0f);
    //outColor = vec4(1.0f, 0.0f, 1.0f, 1.0f);
    //outColor = vec4(texture2D(depthNormalMap, fragUv).rgb, 1.0f);
    //outColor = vec4(fragDist, otherDist, lengthInside, 1.0f);
}
