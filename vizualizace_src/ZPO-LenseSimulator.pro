#-------------------------------------------------
#
# Project created by QtCreator 2018-03-14T12:49:51
#
#-------------------------------------------------

QT       += core gui opengl multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LenseSimulator
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Enable C++14 standard.
CONFIG += c++14
# Compiler flags for GCC.
gcc:QMAKE_CXXFLAGS += -Wall -Werror -pedantic

# Output the binary into "bin" directory.
DESTDIR = ../bin

# Headers are in "include" directory.
INCLUDEPATH += include

# Set path for sources, ui files and includes.
SRC_PATH = ./src
UI_PATH = ./src
INCLUDE_PATH = ./include

SOURCES += \
    $${SRC_PATH}/LenseSimulator.cpp \
    $${SRC_PATH}/MainWindow.cpp \
    $${SRC_PATH}/Util.cpp \
    $${SRC_PATH}/LenseSimulatorApp.cpp \
    $${SRC_PATH}/RenderArea.cpp \
    $${SRC_PATH}/Plane.cpp \
    $${SRC_PATH}/OpenGL.cpp \
    src/LenseEngine.cpp \
    src/Keyboard.cpp \
    src/Mouse.cpp \
    src/ImageSrc.cpp \
    src/ModelMatrix.cpp \
    src/WebcamSrc.cpp \
    src/SceneCamera.cpp \
    src/Cube.cpp \
    src/DummyLense.cpp \
    src/Material.cpp \
    src/SphericalLense.cpp \
    src/ParamSphere.cpp \
    src/ModelLense.cpp \
    src/ModelMesh.cpp

HEADERS += \
        $${INCLUDE_PATH}/MainWindow.h \
    $${INCLUDE_PATH}/Types.h \
    $${INCLUDE_PATH}/Util.h \
    $${INCLUDE_PATH}/LenseSimulatorApp.h \
    $${INCLUDE_PATH}/RenderArea.h \
    $${INCLUDE_PATH}/Timer.h \
    $${INCLUDE_PATH}/Plane.h \
    $${INCLUDE_PATH}/Renderable.h \
    $${INCLUDE_PATH}/OpenGL.h \
    include/LenseEngine.h \
    include/Mouse.h \
    include/Keyboard.h \
    include/ImageSrc.h \
    include/ModelMatrix.h \
    include/WebcamSrc.h \
    include/SceneCamera.h \
    include/SceneObject.h \
    include/Cube.h \
    include/Lense.h \
    include/DummyLense.h \
    include/Material.h \
    include/Config.h \
    include/SphericalLense.h \
    include/ParamSphere.h \
    include/ModelLense.h \
    include/ModelMesh.h

FORMS += \
        $${UI_PATH}/MainWindow.ui

RESOURCES += \
    resources/resources.qrc

DISTFILES += \
    src/shaders/simple.frag \
    src/shaders/simple.vert \
    src/shaders/srcImage.frag \
    src/shaders/srcImage.vert \
    src/shaders/lense.frag \
    src/shaders/lense.vert \
    src/shaders/depthNormal.frag \
    src/shaders/depthNormal.vert \
    src/shaders/lenseChroma.frag

