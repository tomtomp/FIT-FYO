Popis
=====

Tento projekt byl vytvořen do  předměty Fyzikální Optika (FYO) na Fakultě Informačních Technologií na Vysokém Učení Technickém v Brně. Tématem byly aberace čoček.

Kořenová složka obsahuje: 
  * Text písemné práce            : prace.pdf
  * Zdrojový text písemné práce   : adresář prace_src
  * Prezentační slidy             : prezentace.pdf
  * Zdrojový text prezentace      : adresář prezentace_src
  * Vizualizace                   : vizualizace.zip
  * Zdrojové kódy vizualizace     : adresář vizualizace_src

Archiv vizualizace.zip obsahuje přeložený program pro platformu Windows. Pro jeho spouštění je nutné pouze archiv rozbalit a spustit soubor LenseSimulator.exe .

Autorem této práce je: 
  Tomáš Polášek (xpolas34@stud.fit.vutbr.cz)

